import app from './app'
import config from './config'
import 'reflect-metadata'
import './database'

app.listen(config.app.port, () => {
  console.log(`Server is running at port ${config.app.port}!!!`)
})
