import { Repository, EntityRepository } from 'typeorm'
import MovimentacaoUsuarios from '../models/MovimentacaoUsuarios'

@EntityRepository(MovimentacaoUsuarios)
export default class MovimentacaoUsuariosRepository extends Repository<MovimentacaoUsuarios> {
}
