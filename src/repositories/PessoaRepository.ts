import { Repository, EntityRepository } from 'typeorm'
import Pessoa from '../models/Pessoa'

@EntityRepository(Pessoa)
export default class PessoaRepository extends Repository<Pessoa> {
}
