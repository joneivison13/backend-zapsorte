import { Repository, EntityRepository } from 'typeorm'
import Titulo from '../models/Titulo'

@EntityRepository(Titulo)
export default class TituloRepository extends Repository<Titulo> {}
