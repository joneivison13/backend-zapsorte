import { Repository, EntityRepository } from 'typeorm'
import UsuarioParceiroParceiro from '../models/UsuarioParceiroParceiro'

@EntityRepository(UsuarioParceiroParceiro)
export default class UsuarioParceiroParceiroRepository extends Repository<UsuarioParceiroParceiro> {}
