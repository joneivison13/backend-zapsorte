import { Repository, EntityRepository } from 'typeorm'
import SaldoUsuario from '../models/SaldoUsuario'

@EntityRepository(SaldoUsuario)
export default class SaldoUsuarioRepository extends Repository<SaldoUsuario> {
}
