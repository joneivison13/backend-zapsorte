import { Repository, EntityRepository } from 'typeorm'
import CobrancaTerceiros from '../models/CobrancaTerceiros'

@EntityRepository(CobrancaTerceiros)
export default class CobrancaTerceirosRepository extends Repository<CobrancaTerceiros> {
}
