import { Repository, EntityRepository } from 'typeorm'
import UsuarioParceiroCaixaParceiro from '../models/UsuarioParceiroCaixaParceiro'

@EntityRepository(UsuarioParceiroCaixaParceiro)
export default class UsuarioParceiroCaixaParceiroRepository extends Repository<UsuarioParceiroCaixaParceiro> {
}
