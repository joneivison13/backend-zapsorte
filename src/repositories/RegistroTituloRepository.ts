import { Repository, EntityRepository } from 'typeorm'
import RegistroTitulo from '../models/RegistroTitulo'

@EntityRepository(RegistroTitulo)
export default class RegistrarTituloRepository extends Repository<RegistroTitulo> {
}
