import { Repository, EntityRepository } from 'typeorm'
import RecargaCelular from '../models/RecargaCelular'

@EntityRepository(RecargaCelular)
export default class RecargaCelularRepository extends Repository<RecargaCelular> {
}
