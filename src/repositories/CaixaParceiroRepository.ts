import { Repository, EntityRepository } from 'typeorm'
import CaixaParceiro from '../models/CaixaParceiro'

@EntityRepository(CaixaParceiro)
export default class CaixaParceiroRepository extends Repository<CaixaParceiro> {
}
