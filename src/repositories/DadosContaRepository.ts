import { Repository, EntityRepository } from 'typeorm'
import DadosConta from '../models/DadosConta'

@EntityRepository(DadosConta)
export default class DadosContaRepository extends Repository<DadosConta> {
}
