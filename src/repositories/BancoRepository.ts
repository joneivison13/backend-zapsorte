import { Repository, EntityRepository } from 'typeorm'
import Banco from '../models/Banco'

@EntityRepository(Banco)
export default class BancoRepository extends Repository<Banco> {
}
