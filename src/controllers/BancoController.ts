import { Request, Response } from 'express'
import BancoService from '../services/BancoService'

class BancoController {
  async store (request: Request, response: Response) {
    try {
      return BancoService.store(request, response)
    } catch (err) {
      console.log(err.message)
      
    }
  }

  async update (request: Request, response: Response) {
    try {
      return BancoService.update(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }

  async get (request: Request, response: Response) {
    try {
      return BancoService.get(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }

  async delete (request: Request, response: Response) {
    try {
      return BancoService.delete(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new BancoController()
