import { Request, Response } from "express";
import EnderecoService from "../services/EnderecoService";

class EnderecoController {
  async getByCep(request: Request, response: Response) {
    return EnderecoService.getByCep(request, response);
  }

  async store(
    cep: string,
    logradouro: string,
    bairro: string,
    localidade: string,
    uf: string
  ) {
    return EnderecoService.store(cep, logradouro, bairro, localidade, uf);
  }

  async get(request: Request, response: Response) {
    return EnderecoService.get(request, response);
  }
}

export default new EnderecoController();
