import { Request, Response } from 'express'
import TituloService from '../services/TituloService'

class TituloController {
  async store (request: Request, response: Response) {
    try {
      return TituloService.store(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }

  async update (request: Request, response: Response) {
    try {
      return TituloService.update(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }

  async get (request: Request, response: Response) {
    try {
      return TituloService.get(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }

  async delete (request: Request, response: Response) {
    try {
      return TituloService.delete(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new TituloController()
