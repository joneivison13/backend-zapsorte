import { Request, Response } from 'express'
import RegistroTituloService from '../services/RegistroTituloService'

class RegistroTituloController {
  async store (request: Request, response: Response) {
    try {
      return RegistroTituloService.store(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }

  async getByUsuario (request: Request, response: Response) {
    try {
      return RegistroTituloService.getByUsuario(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }

  async getGanhadores (request: Request, response: Response) {
    try {
      return RegistroTituloService.getGanhadores(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new RegistroTituloController()
