import { Request, Response } from 'express'
import UsuarioParceiroService from '../services/UsuarioParceiroService'
import ParceiroService from '../services/ParceiroService'

class UsuarioParceiroController {
  async store (request: Request, response: Response) {
    try {
      return UsuarioParceiroService.store(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }

  async getByCNPJ (request: Request, response: Response) {
    try {
      return UsuarioParceiroService.getByCNPJ(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }

  async getParceiros (request: Request, response: Response) {
    try {
      return ParceiroService.get(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new UsuarioParceiroController()
