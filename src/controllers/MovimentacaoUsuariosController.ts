import { Request, Response } from 'express'
import MovimentacaoUsuariosService from '../services/MovimentacaoUsuariosService'

class MovimentacaoUsuarios {
  async store (request: Request, response: Response) {
    try {
      return MovimentacaoUsuariosService.store(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }
  async storeTransferencia (request: Request, response: Response) {
    try {
      return MovimentacaoUsuariosService.storeTransferencia(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }
  async getHistorico (request: Request, response: Response) {
    try {
      return MovimentacaoUsuariosService.getHistorico(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new MovimentacaoUsuarios()
