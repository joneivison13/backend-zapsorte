import { Request, Response } from 'express'
import SMSService from '../services/SMSService'

class SMSController {
  async gerarTrocoNaoCadastrado (request: Request, response: Response) {
    try {
      return SMSService.gerarTrocoNaoCadastrado(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new SMSController()
