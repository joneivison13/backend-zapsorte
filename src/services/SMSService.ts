import { Request, Response } from 'express'
import axios from 'axios'
import app from '../config'

class SMSService {
  async gerarTrocoNaoCadastrado (request: Request, response: Response) {
    try {
      const { nome, valor, celular } = request.body;

      const texto = `Baixe agora mesmo o Zaptroco e venha fazer parte de uma solução financeira feita para facilitar seu dia-a-dia!\n\nPassou troco: ${nome}\nValor troco: ${valor}\n\nhttp://www.zaptroco.com.br/zaptrocoapps`

      const { data } = await axios({
        url: app.services.smsdev.url + `/v1/send?key=${app.services.smsdev.token}&type=9&number=${celular}&msg=${encodeURIComponent(texto)}&refer=${encodeURIComponent('ZapTroco')}`,
        method: 'GET',
        headers: {}
      })

      response.json({ data })
    } catch (err) {
      console.log(err)
      response.json({ err, msg: 'Houve uma imprevisto no envio do SMS, tente novamente mais tarde...' })
    }
  }
}

export default new SMSService()
