import { Request, Response } from 'express'
import TituloRepository from '../repositories/TituloRepository'
import ParceiroRepository from '../repositories/ParceiroRepository'
import { getCustomRepository } from 'typeorm'
import LoginUsuarioRepository from '../repositories/LoginUsuarioRepository'
import RegistrarTituloRepository from '../repositories/RegistroTituloRepository'

class TituloService {
  async store (request: Request, response: Response) {
    try {
      const { fkIdParceiro, numeroTitulo, dataLancamento, dataVencimento } = request.body

      const repo = getCustomRepository(TituloRepository)
      const repoU = getCustomRepository(ParceiroRepository)

      const existU = await repoU.findOne({idParceiro: fkIdParceiro})
      if (!existU) return response.json({message: 'Este usuário não existe!'})

      const exist = await repo.findOne({numeroTitulo: numeroTitulo, dataVencimento: dataVencimento})
      if (exist) return response.json({message: 'Este título já existe!'})

      const titulo = await repo.save({
        fkIdParceiro, numeroTitulo, dataLancamento, dataVencimento
      })

      response.json(titulo)
    } catch (err) {
      console.log(err.message)
    }
  }

  async update (request: Request, response: Response) {
    try {
      const { idTitulo, numeroTitulo, dataLancamento, dataVencimento } = request.body

      const repo = getCustomRepository(TituloRepository)

      const { affected } = await repo.update(idTitulo, {
        numeroTitulo, dataLancamento, dataVencimento
      })

      response.json({ affected })
    } catch (err) {
      console.log(err.message)
    }
  }

  async get (request: Request, response: Response) {
    try {
      const repo = getCustomRepository(TituloRepository)
      const resp = await repo.find()

      response.json(resp)
    } catch (err) {
      console.log(err.message)
    }
  }

  async delete (request: Request, response: Response) {
    try {
      const { idTitulo } = request.params
      const repo = getCustomRepository(TituloRepository)
      const repoR = getCustomRepository(RegistrarTituloRepository)

      const existApp = await repoR.findOne({fkIdTitulo: idTitulo})
      if (existApp) { return response.status(401).json({message: 'Existe associação entre algum usuario e este título'}) }

      await repo.delete(idTitulo)

      response.json({message: 'Excluído com sucesso!'})
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new TituloService()
