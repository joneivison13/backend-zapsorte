import { Request, Response } from "express";
import axios from "axios";
import EnderecoRepository from "../repositories/EnderecoRepository";
import PessoaEnderecoRepository from "../repositories/PessoaEnderecoRepository";
import { getCustomRepository } from "typeorm";

class EnderecoService {
  async store(
    cep: string,
    logradouro: string,
    bairro: string,
    localidade: string,
    uf: string
  ) {
    try {
      const repoEndereco = getCustomRepository(EnderecoRepository);
      let endereco = await repoEndereco.findOne({ cep });

      if (!endereco) {
        endereco = await repoEndereco.save({
          cep,
          logradouro,
          bairro,
          localidade,
          uf,
        });
      }

      return endereco;
    } catch (err) {
      console.log(err.message);
    }
  }

  async getByCep(request: Request, response: Response) {
    try {
      const { cep } = request.params;

      const repo = getCustomRepository(EnderecoRepository);
      const endereco = await repo.findOne({ cep });

      if (!endereco) {
        const { data } = await axios.get(
          "https://viacep.com.br/ws/" + cep + "/json/"
        );
        response.json(data);
      }

      response.json(endereco);
    } catch (err) {
      console.log(err.message);
    }
  }

  async get(request: Request, response: Response) {
    try {
      const { idUsuario } = request.params;
      const repoEndereco = getCustomRepository(EnderecoRepository);
      const repoPessoaEndereco = getCustomRepository(PessoaEnderecoRepository);

      const pessoaEndereco = await repoPessoaEndereco.findOne({
        where: [{ fkIdPessoa: idUsuario }],
      });

      if (!pessoaEndereco) {
        return response.status(404).json({
          error: true,
          message: "Voçê ainda não cadastrou nenhum endereço.",
        });
      }

      const endereco: any = await repoEndereco.findOne({
        where: [{ idEndereco: pessoaEndereco.fkIdEndereco }],
      });
      endereco.complemento = pessoaEndereco.complemento;
      // console.log({ endereco, complemento: pessoaEndereco.complemento });
      return response.json({ data: { endereco } });
    } catch (err) {
      console.log(err, Object.keys(err), err.name);
    }
  }
}

export default new EnderecoService();
