import UsuarioAppNivelAcessoRepository from '../repositories/UsuarioAppNivelAcessoRepository'
import { getCustomRepository } from 'typeorm'

class UsuarioAppNivelAcessoService {
  async store (fkIdNivelAcesso: number, fkIdLoginUsuario: number) {
    try {
      const repo = getCustomRepository(UsuarioAppNivelAcessoRepository)
      await repo.delete({ fkIdLoginUsuario, fkIdNivelAcesso })

      return await repo.save({
        fkIdLoginUsuario,
        fkIdNivelAcesso
      })
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new UsuarioAppNivelAcessoService()
