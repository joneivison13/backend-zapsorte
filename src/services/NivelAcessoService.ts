import { Request, Response } from 'express'
import NivelAcessoRepository from '../repositories/NivelAcessoRepository'
import UsuarioAppNivelAcessoRepository from '../repositories/UsuarioAppNivelAcessoRepository'
import UsuarioParceiroNivelAcessoRepository from '../repositories/UsuarioParceiroNivelAcessoRepository'
import { getCustomRepository } from 'typeorm'

class NivelAcessoService {
  async store (request: Request, response: Response) {
    try {
      const { role } = request.body

      const repo = getCustomRepository(NivelAcessoRepository)

      const nivelAcesso = await repo.save({
        role
      })

      response.json(nivelAcesso)
    } catch (err) {
      console.log(err.message)
    }
  }

  async update (request: Request, response: Response) {
    try {
      const { idNivelAcesso, role } = request.body

      const repo = getCustomRepository(NivelAcessoRepository)

      const { affected } = await repo.update(idNivelAcesso, {
        role
      })

      response.json({ affected })
    } catch (err) {
      console.log(err.message)
    }
  }

  async get (request: Request, response: Response) {
    try {
      const repo = getCustomRepository(NivelAcessoRepository)
      const resp = await repo.find()

      response.json(resp)
    } catch (err) {
      console.log(err.message)
    }
  }

  async delete (request: Request, response: Response) {
    try {
      const { idNivelAcesso } = request.params
      const repo = getCustomRepository(NivelAcessoRepository)
      const repoUsuApp = getCustomRepository(UsuarioAppNivelAcessoRepository)
      const repoUsuPar = getCustomRepository(UsuarioParceiroNivelAcessoRepository)

      const existApp = await repoUsuApp.findOne({ fkIdNivelAcesso: Number(idNivelAcesso) })
      if (existApp) { response.status(401).send('Existe usuário app associado a este nível de acesso') }

      const existPar = await repoUsuPar.findOne({ fkIdNivelAcesso: Number(idNivelAcesso) })
      if (existPar) { response.status(401).send('Existe usuário parceiro associado a este nível de acesso') }

      const resp = await repo.delete(idNivelAcesso)

      response.json(resp)
    } catch (err) {
      console.log(err.message)
    }
  }

  async getDefaultApp () {
    try {
      const repo = getCustomRepository(NivelAcessoRepository)
      return await repo.findOne({ role: 'APP' })
    } catch (err) {
      console.log(err.message)
    }
  }

  async getDefaultParceiro () {
    try {
      const repo = getCustomRepository(NivelAcessoRepository)
      return await repo.findOne({ role: 'PARCEIRO' })
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new NivelAcessoService()
