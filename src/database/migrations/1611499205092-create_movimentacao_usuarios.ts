import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createMovimentacaoUsuarios1611499205092 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: 'movimentacao_usuarios',
            columns: [{
                name: 'id_movimentacao_usuarios',
                type: 'int',
                isPrimary: true,
                isNullable: false,
                isGenerated: true,
                generationStrategy: 'increment'
              }, {
                name: 'fk_id_tipo_movimentacao',
                type: 'int',
                isNullable: false
              }, {
                name: 'fk_pagador',
                type: 'int',
                isNullable: true
              }, {
                name: 'fk_recebedor',
                type: 'int',
                isNullable: true
              }, {
                name: 'descricao_movimentacao',
                type: 'varchar',
                isNullable: true
              }, {
                name: 'valor_movimentacao',
                type: 'decimal',
                precision: 11,
                scale: 2,
                isNullable: false
              }, {
                name: 'data_movimentacao',
                type: 'date',
                isNullable: false,
                default: 'now()'
              }
            ]
          })
      
          await queryRunner.createTable(table, true)
      
          const fk1 = new TableForeignKey({
            columnNames: ['fk_id_tipo_movimentacao'],
            referencedColumnNames: ['id_tipo_movimentacao'],
            referencedTableName: 'tipo_movimentacao',
            onDelete: 'CASCADE'
          })

          const fk2 = new TableForeignKey({
            columnNames: ['fk_pagador'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
          })

          const fk3 = new TableForeignKey({
            columnNames: ['fk_recebedor'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
          })
      
          await queryRunner.createForeignKeys('movimentacao_usuarios', [fk1, fk2, fk3])
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('movimentacao_usuarios')
    }

}
