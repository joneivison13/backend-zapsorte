import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class createRecargaCelular1611496389013 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: 'recarga_celular',
            columns: [
              {
                name: 'id_recarga_celular',
                type: 'int',
                isPrimary: true,
                isNullable: false,
                isGenerated: true,
                generationStrategy: 'increment'
              }, {
                name: 'operadora',
                type: 'varchar',
                isNullable: false
              }, {
                name: 'valor_recarga',
                type: 'decimal',
                precision: 11,
                scale: 2,
                isNullable: false
              }, {
                name: 'numero_celular',
                type: 'varchar',
                length: '11',
                isNullable: false
              }, {
                name: 'criado_em',
                type: 'date',
                isNullable: false,
                default: 'now()'
              }, {
                name: 'atualizado_em',
                type: 'date',
                default: 'now()'
              }
            ]
          })
      
          await queryRunner.createTable(table, true)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('recarga_celular')
    }

}
