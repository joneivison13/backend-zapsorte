import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createCobrancaTerceiros1611499216769 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: 'cobranca_terceiros',
            columns: [{
                name: 'id_cobranca_terceiros',
                type: 'int',
                isPrimary: true,
                isNullable: false,
                isGenerated: true,
                generationStrategy: 'increment'
              }, {
                name: 'fk_usuario_entregador',
                type: 'int',
                isNullable: false
              }, {
                name: 'fk_movimentacao',
                type: 'int',
                isNullable: false
              }, {
                name: 'cobranca_terceiros',
                type: 'date',
                isNullable: false,
                default: 'now()'
              }
            ]
          })
      
          await queryRunner.createTable(table, true)
    
          const fk1 = new TableForeignKey({
            columnNames: ['fk_usuario_entregador'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
          })

          const fk2 = new TableForeignKey({
            columnNames: ['fk_movimentacao'],
            referencedColumnNames: ['id_movimentacao_usuarios'],
            referencedTableName: 'movimentacao_usuarios',
            onDelete: 'CASCADE'
          })
      
          await queryRunner.createForeignKeys('cobranca_terceiros', [fk1, fk2])
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('cobranca_terceiros')
    }

}
