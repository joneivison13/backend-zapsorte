import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class createTableUsuarioParceiro1599687732475 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    const table = new Table({
      name: 'usuario_parceiro',
      columns: [
        {
          name: 'id_usuario_parceiro',
          type: 'int',
          isPrimary: true,
          isNullable: false,
          isGenerated: true,
          generationStrategy: 'increment'
        }, {
          name: 'nome_usuario',
          type: 'varchar',
          length: '14'
        }, {
          name: 'cpf',
          type: 'varchar',
          length: '14'
        }, {
          name: 'password',
          type: 'varchar',
          isNullable: false,
          length: '255'
        }, {
          name: 'ativo',
          type: 'boolean',
          isNullable: false
        }, {
          name: 'criado_em',
          type: 'date',
          isNullable: false,
          default: 'now()'
        }, {
          name: 'atualizado_em',
          type: 'date',
          default: 'now()'
        }
      ]
    })

    await queryRunner.createTable(table, true)
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('usuario_parceiro')
  }
}
