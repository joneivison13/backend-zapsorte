import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class createTableRegistroTitulo1599687796336 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    const table = new Table({
      name: 'registro_titulo',
      columns: [{
        name: 'id_registro_titulo',
        type: 'int',
        isPrimary: true,
        isNullable: false,
        isGenerated: true,
        generationStrategy: 'increment'
      }, {
        name: 'fk_id_login_usuario',
        type: 'int',
        isNullable: false
      }, {
        name: 'fk_id_titulo',
        type: 'int',
        isNullable: false
      }, {
        name: 'data_registro',
        type: 'date',
        isNullable: false
      }, {
        name: 'ganhador',
        type: 'varchar',
        isNullable: false,
        default: "'N'"
      }]
    })

    await queryRunner.createTable(table)

    const fkTitulo = new TableForeignKey({
      columnNames: ['fk_id_titulo'],
      referencedColumnNames: ['id_titulo'],
      referencedTableName: 'titulo',
      onDelete: 'CASCADE'
    })

    const fkUsuarioApp = new TableForeignKey({
      columnNames: ['fk_id_login_usuario'],
      referencedColumnNames: ['id_login_usuario'],
      referencedTableName: 'login_usuario',
      onDelete: 'CASCADE'
    })

    await queryRunner.createForeignKeys('registro_titulo', [fkTitulo, fkUsuarioApp])
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('registro_titulo')
  }
}
