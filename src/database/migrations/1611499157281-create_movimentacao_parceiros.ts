import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createMovimentacaoParceiros1611499157281 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: 'movimentacao_parceiros',
            columns: [{
                name: 'id_movimentacao_parceiros',
                type: 'int',
                isPrimary: true,
                isNullable: false,
                isGenerated: true,
                generationStrategy: 'increment'
              }, {
                name: 'fk_id_tipo_movimentacao',
                type: 'int',
                isNullable: false
              }, {
                name: 'fk_id_login_usuario',
                type: 'int',
                isNullable: false
              }, {
                name: 'fk_id_parceiro',
                type: 'int',
                isNullable: false
              }, {
                name: 'descricao_movimentacao',
                type: 'varchar',
                isNullable: false
              }, {
                name: 'valor_movimentacao',
                type: 'decimal',
                precision: 11,
                scale: 2,
                isNullable: false
              }, {
                name: 'data_movimentacao',
                type: 'date',
                isNullable: false,
                default: 'now()'
              }
            ]
          })
      
          await queryRunner.createTable(table, true)
      
          const fk1 = new TableForeignKey({
            columnNames: ['fk_id_tipo_movimentacao'],
            referencedColumnNames: ['id_tipo_movimentacao'],
            referencedTableName: 'tipo_movimentacao',
            onDelete: 'CASCADE'
          })

          const fk2 = new TableForeignKey({
            columnNames: ['fk_id_login_usuario'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
          })

          const fk3 = new TableForeignKey({
            columnNames: ['fk_id_parceiro'],
            referencedColumnNames: ['id_parceiro'],
            referencedTableName: 'parceiro',
            onDelete: 'CASCADE'
          })
      
          await queryRunner.createForeignKeys('movimentacao_parceiros', [fk1, fk2, fk3])
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('movimentacao_parceiros')
    }

}
