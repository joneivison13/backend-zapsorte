import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class createTablePessoa1599687456147 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    const table = new Table({
      name: 'pessoa',
      columns: [
        {
          name: 'id_pessoa',
          type: 'int',
          isPrimary: true,
          isNullable: false,
          isGenerated: true,
          generationStrategy: 'increment'
        }, {
          name: 'fk_id_login_usuario',
          type: 'int',
          isNullable: false
        }, {
          name: 'nome_pessoa',
          type: 'varchar',
          isNullable: false
        }, {
          name: 'ultimo_nome_pessoa',
          type: 'varchar',
          isNullable: false
        }, {
          name: 'data_nascimento',
          type: 'date',
          isNullable: true
        }, {
          name: 'genero',
          type: 'varchar',
          isNullable: true
        }, {
          name: 'identidade',
          type: 'varchar',
          length: '20',
          isNullable: true
        }, {
          name: 'criado_em',
          type: 'date',
          isNullable: false,
          default: 'now()'
        }, {
          name: 'atualizado_em',
          type: 'date',
          default: 'now()'
        }
      ]
    })

    await queryRunner.createTable(table, true)

    const fkUser = new TableForeignKey({
      columnNames: ['fk_id_login_usuario'],
      referencedColumnNames: ['id_login_usuario'],
      referencedTableName: 'login_usuario',
      onDelete: 'CASCADE'
    })

    await queryRunner.createForeignKeys('pessoa', [fkUser])
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('pessoa')
  }
}
