import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createPessoaEndereco1611494536974 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: 'pessoa_endereco',
            columns: [
              {
                name: 'fk_id_pessoa',
                type: 'int',
                isNullable: false
              }, {
                name: 'fk_id_endereco',
                type: 'int',
                isNullable: false
              }, {
                name: 'complemento',
                type: 'varchar',
                isNullable: false
              }, {
                name: 'latitude',
                type: 'varchar',
                length: '100',
                isNullable: true
              }, {
                name: 'longitude',
                type: 'varchar',
                length: '100',
                isNullable: true
              }, {
                name: 'criado_em',
                type: 'date',
                isNullable: false,
                default: 'now()'
              }, {
                name: 'atualizado_em',
                type: 'date',
                default: 'now()'
              }
            ]
          })
      
          await queryRunner.createTable(table, true)
      
          const fkPessoa = new TableForeignKey({
            columnNames: ['fk_id_pessoa'],
            referencedColumnNames: ['id_pessoa'],
            referencedTableName: 'pessoa',
            onDelete: 'CASCADE'
          })
      
          const fkEndereco = new TableForeignKey({
            columnNames: ['fk_id_endereco'],
            referencedColumnNames: ['id_endereco'],
            referencedTableName: 'endereco',
            onDelete: 'CASCADE'
          })
      
          await queryRunner.createForeignKeys('pessoa_endereco', [fkPessoa, fkEndereco])
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('pessoa_endereco')
    }

}
