import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class createTableNivelAcesso1599687691907 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    const table = new Table({
      name: 'nivel_acesso',
      columns: [{
        name: 'id_nivel_acesso',
        type: 'int',
        isPrimary: true,
        isNullable: false,
        isGenerated: true,
        generationStrategy: 'increment'
      }, {
        name: 'role',
        type: 'varchar',
        length: '100',
        isNullable: false
      }]
    })

    await queryRunner.createTable(table)
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('nivel_acesso')
  }
}
