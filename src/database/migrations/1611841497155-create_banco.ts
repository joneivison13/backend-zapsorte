import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class createBanco1611841497155 implements MigrationInterface {

      public async up (queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
          name: 'banco',
          columns: [{
            name: 'id_banco',
            type: 'int',
            isPrimary: true,
            isNullable: false,
            isGenerated: true,
            generationStrategy: 'increment'
          }, {
            name: 'nome_banco',
            type: 'varchar',
            length: '100',
            isNullable: false
          }, {
            name: 'codigo_febrace',
            type: 'int',
            isNullable: false
          }]
        })
    
        await queryRunner.createTable(table)
      }
    
      public async down (queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('banco')
      }

}
