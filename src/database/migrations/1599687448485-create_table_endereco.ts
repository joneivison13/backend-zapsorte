import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class createTableEndereco1599687448485 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    const table = new Table({
      name: 'endereco',
      columns: [
        {
          name: 'id_endereco',
          type: 'int',
          isPrimary: true,
          isNullable: false,
          isGenerated: true,
          generationStrategy: 'increment'
        }, {
          name: 'cep',
          type: 'varchar',
          isNullable: false,
          length: '8'
        }, {
          name: 'logradouro',
          type: 'varchar',
          isNullable: false,
          length: '200'
        }, {
          name: 'bairro',
          type: 'varchar',
          length: '80',
          isNullable: false
        }, {
          name: 'localidade',
          type: 'varchar',
          length: '100',
          isNullable: false
        }, {
          name: 'uf',
          type: 'varchar',
          length: '2',
          isNullable: false
        }
      ]
    })

    await queryRunner.createTable(table, true)
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('endereco')
  }
}
