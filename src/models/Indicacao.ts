import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn } from 'typeorm'
import LoginUsuario from './LoginUsuario'

@Entity({ name: 'indicacao' })
export default class Indicacao {
  @PrimaryGeneratedColumn('increment', { name: 'id_indicacao' })
  idindicacao: number

  @Column({
    name: 'fk_indicador',
    nullable: false
  })
  fkIndicador: number

  @Column({
    name: 'fk_indicado',
    nullable: false
  })
  fkIndicado: number

  @ManyToOne(type => LoginUsuario, indicacao => Indicacao)
  @JoinColumn({ name: 'fk_indicador' })
  indicador: LoginUsuario;

  @ManyToOne(type => LoginUsuario, indicacao => Indicacao)
  @JoinColumn({ name: 'fk_indicado' })
  indicado: LoginUsuario;

  @CreateDateColumn({ name: 'criado_em' })
  criadoEm: Date;

  @UpdateDateColumn({ name: 'atualizado_em' })
  atualizadoEm: Date;
}
