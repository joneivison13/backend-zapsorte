import { Entity, Column, ManyToOne, JoinColumn } from 'typeorm'
import Parceiro from './Parceiro';
import UsuarioParceiro from './UsuarioParceiro';

@Entity({ name: 'usuario_parceiro_parceiro' })
export default class UsuarioParceiroParceiro {
  @Column({
    name: 'fk_id_usuario_parceiro',
    nullable: false,
    primary: true
  })
  fkIdUsuarioParceiro: number

  @Column({
    name: 'fk_id_parceiro',
    nullable: false,
    primary: true
  })
  fkIdParceiro: number

  @ManyToOne(type => UsuarioParceiro, usuarioParceiroParceiro => UsuarioParceiroParceiro)
  @JoinColumn({ name: 'fk_id_login_usuario' })
  usuarioParceiro: UsuarioParceiro;

  @ManyToOne(type => Parceiro, usuarioParceiroParceiro => UsuarioParceiroParceiro)
  @JoinColumn({ name: 'fk_id_login_usuario' })
  parceiro: Parceiro;
}
