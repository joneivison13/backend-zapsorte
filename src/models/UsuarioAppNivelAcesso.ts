import { Entity, Column, ManyToOne, JoinColumn } from 'typeorm'
import UsuarioApp from './LoginUsuario'
import NivelAcesso from './NivelAcesso'

@Entity({ name: 'usuario_app_nivel_acesso' })
export default class UsuarioAppNivelAcesso {
  @Column({
    name: 'fk_id_login_usuario',
    nullable: false,
    primary: true
  })
  fkIdLoginUsuario: number

  @Column({
    name: 'fk_id_nivel_acesso',
    nullable: false,
    primary: true
  })
  fkIdNivelAcesso: number

  @ManyToOne(type => UsuarioApp, usuarioAppNiveisAcesso => UsuarioAppNivelAcesso)
  @JoinColumn({ name: 'fk_id_login_usuario' })
  usuarioApp: UsuarioApp;

  @ManyToOne(type => NivelAcesso, usuarioAppNiveisAcesso => UsuarioAppNivelAcesso)
  @JoinColumn({ name: 'fk_id_nivel_acesso' })
  nivelAcesso: NivelAcesso;
}
