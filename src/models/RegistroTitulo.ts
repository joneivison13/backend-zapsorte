import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm'
import Titulo from './Titulo'
import LoginUsuario from './LoginUsuario'

@Entity({ name: 'registro_titulo' })
export default class RegistroTitulo {
  @PrimaryGeneratedColumn('increment', { name: 'id_registro_titulo' })
  idRegistroTitulo: number

  @Column({
    name: 'fk_id_login_usuario',
    nullable: false
  })
  fkIdLoginUsuario: number

  @Column({
    name: 'fk_id_titulo',
    nullable: false
  })
  fkIdTitulo: number

  @Column({
    name: 'data_registro',
    nullable: false
  })
  dataRegistro: Date

  @Column({
    nullable: false
  })
  ganhador: string

  @ManyToOne(type => LoginUsuario, registrosTitulo => RegistroTitulo)
  @JoinColumn({ name: 'fk_id_login_usuario' })
  loginUsuario: LoginUsuario;

  @ManyToOne(type => Titulo, registrosTitulo => RegistroTitulo)
  @JoinColumn({ name: 'fk_id_titulo' })
  titulo: Titulo;
}
