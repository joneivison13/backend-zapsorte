import { Entity, PrimaryGeneratedColumn, Column, OneToMany, CreateDateColumn, UpdateDateColumn } from 'typeorm'
import RecargaCelularLoginUsuario from './RecargaCelularLoginUsuario'

@Entity({ name: 'recarga_celular' })
export default class RecargaCelular {
    @PrimaryGeneratedColumn('increment', { name: 'id_recarga_celular' })
    idRecargaCelular: number;

    @Column({
      nullable: false
    })
    operadora: string;

    @Column({
      name: 'valor_recarga',
      nullable: false,
      precision: 11,
      scale: 2
    })
    valorRecarga: number;

    @Column({
      name: 'numero_celular',
      nullable: false
    })
    numeroCelular: string;

    @CreateDateColumn({ name: 'criado_em' })
    criadoEm: Date;

    @UpdateDateColumn({ name: 'atualizado_em' })
    atualizadoEm: Date;

    @OneToMany(type => RecargaCelularLoginUsuario, recargaCelular => RecargaCelular)
    recargasCelularLoginUsuario: RecargaCelularLoginUsuario[];
}
