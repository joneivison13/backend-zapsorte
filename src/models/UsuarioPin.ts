import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn } from 'typeorm'
import LoginUsuario from './LoginUsuario'
import UsuariosPin from './UsuarioPin'

@Entity({ name: 'usuario_pin' })
export default class UsuarioPin {
  @PrimaryGeneratedColumn('increment', { name: 'id_usuario_pin' })
  idUsuarioPin: number

  @Column({
    name: 'fk_id_login_usuario',
    nullable: true
  })
  fkIdLoginUsuario: number

  @Column({
    name: 'pin_usuario',
    unique: true,
    nullable: false
  })
  pinUsuario: string;

  @Column({
    name: 'celular',
    nullable: false
  })
  celular: string;

  @CreateDateColumn({ name: 'criado_em' })
  criadoEm: Date;

  @UpdateDateColumn({ name: 'atualizado_em' })
  atualizadoEm: Date;

  @ManyToOne(type => LoginUsuario, usuariosPin => UsuariosPin)
  @JoinColumn({ name: 'fk_id_login_usuario' })
  loginUsuario: LoginUsuario;
}
