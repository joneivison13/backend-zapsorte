import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm'
import MovimentacaoUsuarios from './MovimentacaoUsuarios'
import MovimentacaoParceiros from './MovimentacaoParceiros'

@Entity({ name: 'tipo_movimentacao' })
export default class TipoMovimentacao {
    @PrimaryGeneratedColumn('increment', { name: 'id_tipo_movimentacao' })
    idTipoMovimentacao: number;

    @Column({
      name: 'descricao_tipo_movimentacao',
      nullable: false
    })
    descricaoTipoMovimentacao: string;

    @OneToMany(type => MovimentacaoUsuarios, tipoMovimentacao => TipoMovimentacao)
    movimentacoesUsuarios: MovimentacaoUsuarios[];

    @OneToMany(type => MovimentacaoParceiros, tipoMovimentacao => TipoMovimentacao)
    movimentacoesParceiros: MovimentacaoParceiros[];
}
