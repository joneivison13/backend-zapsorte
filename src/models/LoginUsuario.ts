import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany } from 'typeorm'
import UsuarioPin from './UsuarioPin'
import Pessoa from './Pessoa'
import UsuarioAppNivelAcesso from './UsuarioAppNivelAcesso'
import RegistroTitulo from './RegistroTitulo'
import RecargaCelularLoginUsuario from './RecargaCelularLoginUsuario'
import DadosConta from './DadosConta'
import SaldoUsuario from './SaldoUsuario'
import Indicacao from './Indicacao'
import MovimentacaoUsuarios from './MovimentacaoUsuarios'
import MovimentacaoParceiros from './MovimentacaoParceiros'
import CobrancaTerceiros from './CobrancaTerceiros'
import CreditoPedagio from './CreditoPedagio'

@Entity({ name: 'login_usuario' }) 
export default class LoginUsuario {
    @PrimaryGeneratedColumn('increment', { name: 'id_login_usuario' })
    idLoginUsuario: number;

    @Column({
      length: 11,
      unique: true,
      name: 'cpf'
    })
    cpf: string;

    @Column({
      length: 11,
      name: 'numero_celular'
    })
    numeroCelular: string;

    @Column({
      name: 'password_usuario',
      nullable: false
    })
    passwordUsuario: string;

    @Column({
      nullable: false
    })
    email: string;

    @Column({
      nullable: false,
      default: true
    })
    ativado: boolean;

    @Column({
      nullable: false,
      precision: 11,
      scale: 2,
      name: 'saldo_atual'
    })
    saldoAtual: number;

    @Column({
      nullable: false,
      precision: 11,
      scale: 2,
      name: 'saldo_pedagio'
    })
    saldoPedagio: number;

    @Column({
      nullable: true
    })
    avatar: string;

    @OneToMany(type => Pessoa, loginUsuario => LoginUsuario)
    pessoas: Pessoa[];

    @OneToMany(type => UsuarioPin, loginUsuario => LoginUsuario)
    usuariosPin: UsuarioPin[];

    @OneToMany(type => UsuarioAppNivelAcesso, loginUsuario => LoginUsuario)
    usuarioAppNiveisAcesso: UsuarioAppNivelAcesso[];

    @OneToMany(type => RegistroTitulo, loginUsuario => loginUsuario)
    registrosTitulo: RegistroTitulo[];

    @OneToMany(type => RecargaCelularLoginUsuario, loginUsuario => loginUsuario)
    recargasCelularLoginUsuario: RecargaCelularLoginUsuario[];

    @OneToMany(type => DadosConta, loginUsuario => loginUsuario)
    dadosConta: DadosConta[];

    @OneToMany(type => SaldoUsuario, loginUsuario => loginUsuario)
    saldoUsuario: SaldoUsuario[];

    @OneToMany(type => CreditoPedagio, loginUsuario => loginUsuario)
    creditoPedagio: CreditoPedagio[];

    @OneToMany(type => Indicacao, loginUsuario => loginUsuario)
    indicador: Indicacao[];

    @OneToMany(type => Indicacao, loginUsuario => loginUsuario)
    indicado: Indicacao[];

    @OneToMany(type => MovimentacaoUsuarios, loginUsuario => loginUsuario)
    pagador: MovimentacaoUsuarios[];

    @OneToMany(type => MovimentacaoUsuarios, loginUsuario => loginUsuario)
    recebedor: MovimentacaoUsuarios[];

    @OneToMany(type => MovimentacaoParceiros, loginUsuario => loginUsuario)
    loginUsuario: MovimentacaoUsuarios[];

    @OneToMany(type => CobrancaTerceiros, loginUsuario => loginUsuario)
    cobrancaTerceiros: CobrancaTerceiros[];

    @CreateDateColumn({ name: 'criado_em' })
    criadoEm: Date;

    @UpdateDateColumn({ name: 'atualizado_em' })
    atualizadoEm: Date;
}
