import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm'
import UsuarioAppNivelAcesso from './UsuarioAppNivelAcesso'
import UsuarioParceiroNivelAcesso from './UsuarioParceiroNivelAcesso'

@Entity({ name: 'nivel_acesso' })
export default class NivelAcesso {
    @PrimaryGeneratedColumn('increment', { name: 'id_nivel_acesso' })
    idNivelAcesso: number;

    @Column({
      nullable: false
    })
    role: string;

    @OneToMany(type => UsuarioAppNivelAcesso, nivelAcesso => NivelAcesso)
    usuarioAppNiveisAcesso: UsuarioAppNivelAcesso[];

    @OneToMany(type => UsuarioParceiroNivelAcesso, nivelAcesso => NivelAcesso)
    usuarioParceiroNiveisAcesso: UsuarioParceiroNivelAcesso[];
}
