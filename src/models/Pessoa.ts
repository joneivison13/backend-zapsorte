import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn, OneToMany } from 'typeorm'
import LoginUsuario from './LoginUsuario'
import Pessoas from './Pessoa'
import PessoaEndereco from './PessoaEndereco'

@Entity({ name: 'pessoa' })
export default class Pessoa {
  @PrimaryGeneratedColumn('increment', { name: 'id_pessoa' })
  idPessoa: number

  @Column({
    name: 'fk_id_login_usuario',
    nullable: false
  })
  fkIdLoginUsuario: number

  @Column({
    name: 'nome_pessoa',
    nullable: false
  })
  nomePessoa: string

  @Column({
    name: 'ultimo_nome_pessoa',
    nullable: false
  })
  ultimoNomePessoa: string

  @Column({
    name: 'data_nascimento',
    nullable: true
  })
  dataNascimento: Date

  @Column({
    nullable: true
  })
  genero: string

  @Column({
    nullable: true
  })
  identidade: string

  @ManyToOne(type => LoginUsuario, pessoas => Pessoas)
  @JoinColumn({ name: 'fk_id_login_usuario' })
  loginUsuario: LoginUsuario[];

  @OneToMany(type => PessoaEndereco, pessoas => Pessoas)
  pessoaEnderecos: PessoaEndereco[];

  @CreateDateColumn({ name: 'criado_em' })
  criadoEm: Date;

  @UpdateDateColumn({ name: 'atualizado_em' })
  atualizadoEm: Date;
}
