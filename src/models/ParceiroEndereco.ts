import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany, ManyToOne, JoinColumn } from 'typeorm'
import Parceiro from './Parceiro'
import Endereco from './Endereco'

@Entity({ name: 'parceiro_endereco' })
export default class ParceiroEndereco {
    @Column({
      name: 'fk_id_parceiro',
      nullable: false,
      primary: true
    })
    fkIdParceiro: number;

    @Column({
      name: 'fk_id_endereco',
      nullable: false,
      primary: true
    })
    fkIdEndereco: number;

    @Column({
      nullable: false,
    })
    complemento: string;

    @Column({
      nullable: true,
    })
    latitude: string;

    @Column({
      nullable: true,
    })
    longitude: string;

    @ManyToOne(type => Parceiro, parceiroEndereco => ParceiroEndereco)
    @JoinColumn({ name: 'fk_id_parceiro' })
    parceiro: Parceiro;

    @ManyToOne(type => Endereco, parceiroEndereco => ParceiroEndereco)
    @JoinColumn({ name: 'fk_id_endereco' })
    enderecos: Endereco;

    @CreateDateColumn({ name: 'criado_em' })
    criadoEm: Date;

    @UpdateDateColumn({ name: 'atualizado_em' })
    atualizadoEm: Date;
}
