import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm'
import MovimentacaoUsuarios from './MovimentacaoUsuarios';
import Transferencia from './Transferencia'

@Entity({ name: 'banco' })
export default class Banco {
    @PrimaryGeneratedColumn('increment', { name: 'id_banco' })
    idBanco: number;

    @Column({
      name: 'nome_banco',
      nullable: false
    })
    nomeBanco: string;

    @Column({
      name: 'codigo_febrace',
      nullable: false
    })
    codigoFebrace: number;

    @OneToMany(type => Transferencia, banco => Banco)
    transferencia: Transferencia[];

    @OneToMany(type => MovimentacaoUsuarios, banco => Banco)
    movimentacaoUsuarios: MovimentacaoUsuarios[];
}
