import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn, OneToMany } from 'typeorm'
import LoginUsuario from './LoginUsuario'

@Entity({ name: 'saldo_usuario' })
export default class SaldoUsuario {
  @PrimaryGeneratedColumn('increment', { name: 'id_saldo_usuario' })
  idSaldoUsuario: number

  @Column({
    name: 'fk_id_login_usuario',
    nullable: false
  })
  fkIdLoginUsuario: number

  @Column({
    name: 'valor_lancado',
    precision: 11,
    scale: 2,
    nullable: false
  })
  valorLancado: number

  @Column({
    name: 'data_lancamento_saldo_usuario',
    nullable: false
  })
  dataLancamentoSaldoUsuario: Date

  @ManyToOne(type => LoginUsuario, saldoUsuario => SaldoUsuario)
  @JoinColumn({ name: 'fk_id_login_usuario' })
  loginUsuario: LoginUsuario;

  @CreateDateColumn({ name: 'criado_em' })
  criadoEm: Date;

  @UpdateDateColumn({ name: 'atualizado_em' })
  atualizadoEm: Date;
}
