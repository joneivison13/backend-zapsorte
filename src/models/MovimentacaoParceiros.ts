import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn, OneToMany } from 'typeorm'
import TipoMovimentacao from './TipoMovimentacao'
import LoginUsuario from './LoginUsuario'

@Entity({ name: 'movimentacao_parceiros' })
export default class MovimentacaoParceiros {
  @PrimaryGeneratedColumn('increment', { name: 'id_movimentacao_parceiros' })
  idMovimentacaoParceiros: number

  @Column({
    name: 'fk_id_tipo_movimentacao',
    nullable: false
  })
  fkIdTipoMovimentacao: number

  @Column({
    name: 'fk_id_login_usuario',
    nullable: false
  })
  fkIdLoginUsuario: number

  @Column({
    name: 'fk_id_parceiro',
    nullable: false
  })
  fkIdParceiro: number

  @Column({
    name: 'descricao_movimentacao',
    nullable: false
  })
  descricaoMovimentacao: string

  @Column({
    name: 'valor_movimentacao',
    nullable: false,
    precision: 11,
    scale: 2
  })
  valorMovimentacao: number

  @Column({
    name: 'data_movimentacao',
    nullable: false
  })
  dataMovimentacao: Date

  @ManyToOne(type => TipoMovimentacao, movimentacaoParceiros => MovimentacaoParceiros)
  @JoinColumn({ name: 'fk_id_tipo_movimentacao' })
  tipoMovimentacao: TipoMovimentacao;

  @ManyToOne(type => LoginUsuario, movimentacaoParceiros => MovimentacaoParceiros)
  @JoinColumn({ name: 'fk_id_login_usuario' })
  loginUsuario: TipoMovimentacao;

  @ManyToOne(type => LoginUsuario, movimentacaoParceiros => MovimentacaoParceiros)
  @JoinColumn({ name: 'fk_id_parceiro' })
  parceiro: TipoMovimentacao;

  @CreateDateColumn({ name: 'criado_em' })
  criadoEm: Date;

  @UpdateDateColumn({ name: 'atualizado_em' })
  atualizadoEm: Date;
}
