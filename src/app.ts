import express from "express";
import cors from "cors";
import routes from "./routes";
import fs from "fs";

const app = express();
const https = require("https");
// const secureServer = https.createServer(
//   {
//     key: fs.readFileSync(
//       "/home/zaptroco/zapsorte-backend/src/sslcert/server.key"
//     ),
//     cert: fs.readFileSync(
//       "/home/zaptroco/zapsorte-backend/src/sslcert/ea7467b80c0b85cc.pem"
//     ),
//     ca: fs.readFileSync(
//       "/home/zaptroco/zapsorte-backend/src/sslcert/ea7467b80c0b85cc.crt"
//     ),
//     g1: fs.readFileSync(
//       "/home/zaptroco/zapsorte-backend/src/sslcert/gd_bundle-g2-g1.crt"
//     ),
//   },
//   app
// );
// secureServer.listen(443, () => {
//   console.log("Secure server is listening on port 443!!");
// });
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use(express.static("tmp"));
app.use(routes);
export default app;
