"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createTableUsuarioPin1599685257485 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'usuario_pin',
            columns: [
                {
                    name: 'id_usuario_pin',
                    type: 'int',
                    isPrimary: true,
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: 'increment'
                }, {
                    name: 'fk_id_login_usuario',
                    type: 'int',
                    isNullable: true
                }, {
                    name: 'pin_usuario',
                    type: 'varchar',
                    isNullable: false
                }, {
                    name: 'celular',
                    type: 'varchar',
                    length: '11',
                    isNullable: false
                }, {
                    name: 'criado_em',
                    type: 'date',
                    isNullable: false,
                    default: 'now()'
                }, {
                    name: 'atualizado_em',
                    type: 'date',
                    default: 'now()'
                }
            ]
        });
        await queryRunner.createTable(table, true);
        const fkUser = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_login_usuario'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
        });
        await queryRunner.createForeignKey('usuario_pin', fkUser);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('usuario_pin');
    }
}
exports.createTableUsuarioPin1599685257485 = createTableUsuarioPin1599685257485;
