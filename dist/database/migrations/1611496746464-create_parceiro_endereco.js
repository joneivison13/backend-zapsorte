"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createParceiroEndereco1611496746464 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'parceiro_endereco',
            columns: [
                {
                    name: 'fk_id_parceiro',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'fk_id_endereco',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'complemento',
                    type: 'varchar',
                    isNullable: false
                }, {
                    name: 'latitude',
                    type: 'varchar',
                    length: '100',
                    isNullable: true
                }, {
                    name: 'longitude',
                    type: 'varchar',
                    length: '100',
                    isNullable: true
                }, {
                    name: 'criado_em',
                    type: 'date',
                    isNullable: false,
                    default: 'now()'
                }, {
                    name: 'atualizado_em',
                    type: 'date',
                    default: 'now()'
                }
            ]
        });
        await queryRunner.createTable(table, true);
        const fkParceiro = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_parceiro'],
            referencedColumnNames: ['id_parceiro'],
            referencedTableName: 'parceiro',
            onDelete: 'CASCADE'
        });
        const fkEndereco = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_endereco'],
            referencedColumnNames: ['id_endereco'],
            referencedTableName: 'endereco',
            onDelete: 'CASCADE'
        });
        await queryRunner.createForeignKeys('parceiro_endereco', [fkParceiro, fkEndereco]);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('parceiro_endereco');
    }
}
exports.createParceiroEndereco1611496746464 = createParceiroEndereco1611496746464;
