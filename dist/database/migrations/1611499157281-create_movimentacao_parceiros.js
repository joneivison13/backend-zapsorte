"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createMovimentacaoParceiros1611499157281 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'movimentacao_parceiros',
            columns: [{
                    name: 'id_movimentacao_parceiros',
                    type: 'int',
                    isPrimary: true,
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: 'increment'
                }, {
                    name: 'fk_id_tipo_movimentacao',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'fk_id_login_usuario',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'fk_id_parceiro',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'descricao_movimentacao',
                    type: 'varchar',
                    isNullable: false
                }, {
                    name: 'valor_movimentacao',
                    type: 'decimal',
                    precision: 11,
                    scale: 2,
                    isNullable: false
                }, {
                    name: 'data_movimentacao',
                    type: 'date',
                    isNullable: false,
                    default: 'now()'
                }
            ]
        });
        await queryRunner.createTable(table, true);
        const fk1 = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_tipo_movimentacao'],
            referencedColumnNames: ['id_tipo_movimentacao'],
            referencedTableName: 'tipo_movimentacao',
            onDelete: 'CASCADE'
        });
        const fk2 = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_login_usuario'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
        });
        const fk3 = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_parceiro'],
            referencedColumnNames: ['id_parceiro'],
            referencedTableName: 'parceiro',
            onDelete: 'CASCADE'
        });
        await queryRunner.createForeignKeys('movimentacao_parceiros', [fk1, fk2, fk3]);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('movimentacao_parceiros');
    }
}
exports.createMovimentacaoParceiros1611499157281 = createMovimentacaoParceiros1611499157281;
