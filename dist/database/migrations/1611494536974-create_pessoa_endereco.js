"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createPessoaEndereco1611494536974 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'pessoa_endereco',
            columns: [
                {
                    name: 'fk_id_pessoa',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'fk_id_endereco',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'complemento',
                    type: 'varchar',
                    isNullable: false
                }, {
                    name: 'latitude',
                    type: 'varchar',
                    length: '100',
                    isNullable: true
                }, {
                    name: 'longitude',
                    type: 'varchar',
                    length: '100',
                    isNullable: true
                }, {
                    name: 'criado_em',
                    type: 'date',
                    isNullable: false,
                    default: 'now()'
                }, {
                    name: 'atualizado_em',
                    type: 'date',
                    default: 'now()'
                }
            ]
        });
        await queryRunner.createTable(table, true);
        const fkPessoa = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_pessoa'],
            referencedColumnNames: ['id_pessoa'],
            referencedTableName: 'pessoa',
            onDelete: 'CASCADE'
        });
        const fkEndereco = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_endereco'],
            referencedColumnNames: ['id_endereco'],
            referencedTableName: 'endereco',
            onDelete: 'CASCADE'
        });
        await queryRunner.createForeignKeys('pessoa_endereco', [fkPessoa, fkEndereco]);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('pessoa_endereco');
    }
}
exports.createPessoaEndereco1611494536974 = createPessoaEndereco1611494536974;
