"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createTableUsuarioParceiroNivelAcesso1599687749968 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'usuario_parceiro_nivel_acesso',
            columns: [{
                    name: 'fk_id_usuario_parceiro',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'fk_id_nivel_acesso',
                    type: 'int',
                    isNullable: false
                }]
        });
        await queryRunner.createTable(table);
        const fkUsuarioApp = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_usuario_parceiro'],
            referencedColumnNames: ['id_usuario_parceiro'],
            referencedTableName: 'usuario_parceiro',
            onDelete: 'CASCADE'
        });
        const fkNivelAcesso = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_nivel_acesso'],
            referencedColumnNames: ['id_nivel_acesso'],
            referencedTableName: 'nivel_acesso',
            onDelete: 'CASCADE'
        });
        await queryRunner.createForeignKeys('usuario_parceiro_nivel_acesso', [fkUsuarioApp, fkNivelAcesso]);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('usuario_parceiro_nivel_acesso');
    }
}
exports.createTableUsuarioParceiroNivelAcesso1599687749968 = createTableUsuarioParceiroNivelAcesso1599687749968;
