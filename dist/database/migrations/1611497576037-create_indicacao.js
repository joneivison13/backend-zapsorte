"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createIndicacao1611497576037 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'indicacao',
            columns: [{
                    name: 'fk_indicador',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'fk_indicado',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'criado_em',
                    type: 'date',
                    isNullable: false,
                    default: 'now()'
                }, {
                    name: 'atualizado_em',
                    type: 'date',
                    default: 'now()'
                }
            ]
        });
        await queryRunner.createTable(table, true);
        const fk1 = new typeorm_1.TableForeignKey({
            columnNames: ['fk_indicador'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
        });
        const fk2 = new typeorm_1.TableForeignKey({
            columnNames: ['fk_indicado'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
        });
        await queryRunner.createForeignKeys('indicacao', [fk1, fk2]);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('indicacao');
    }
}
exports.createIndicacao1611497576037 = createIndicacao1611497576037;
