"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createDadosConta1611497597089 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'dados_conta',
            columns: [{
                    name: 'id_dados_conta',
                    type: 'int',
                    isPrimary: true,
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: 'increment'
                }, {
                    name: 'fk_id_login_usuario',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'agencia',
                    type: 'varchar',
                    isNullable: false
                }, {
                    name: 'conta',
                    type: 'varchar',
                    isNullable: false
                }, {
                    name: 'dvconta',
                    type: 'varchar',
                    isNullable: false
                }, {
                    name: 'criado_em',
                    type: 'date',
                    isNullable: false,
                    default: 'now()'
                }, {
                    name: 'atualizado_em',
                    type: 'date',
                    default: 'now()'
                }
            ]
        });
        await queryRunner.createTable(table, true);
        const fk = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_login_usuario'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
        });
        await queryRunner.createForeignKeys('dados_conta', [fk]);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('dados_conta');
    }
}
exports.createDadosConta1611497597089 = createDadosConta1611497597089;
