"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createRecargaCelularLoginUsuario1611496400833 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'recarga_celular_login_usuario',
            columns: [
                {
                    name: 'fk_id_login_usuario',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'fk_id_recarga_celular',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'criado_em',
                    type: 'date',
                    isNullable: false,
                    default: 'now()'
                }, {
                    name: 'atualizado_em',
                    type: 'date',
                    default: 'now()'
                }
            ]
        });
        await queryRunner.createTable(table, true);
        const fkLoginUsuario = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_login_usuario'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
        });
        const fkRecargCelular = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_recarga_celular'],
            referencedColumnNames: ['id_recarga_celular'],
            referencedTableName: 'recarga_celular',
            onDelete: 'CASCADE'
        });
        await queryRunner.createForeignKeys('recarga_celular_login_usuario', [fkLoginUsuario, fkRecargCelular]);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('recarga_celular_login_usuario');
    }
}
exports.createRecargaCelularLoginUsuario1611496400833 = createRecargaCelularLoginUsuario1611496400833;
