"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createTablePessoa1599687456147 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'pessoa',
            columns: [
                {
                    name: 'id_pessoa',
                    type: 'int',
                    isPrimary: true,
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: 'increment'
                }, {
                    name: 'fk_id_login_usuario',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'nome_pessoa',
                    type: 'varchar',
                    isNullable: false
                }, {
                    name: 'ultimo_nome_pessoa',
                    type: 'varchar',
                    isNullable: false
                }, {
                    name: 'data_nascimento',
                    type: 'date',
                    isNullable: true
                }, {
                    name: 'genero',
                    type: 'varchar',
                    isNullable: true
                }, {
                    name: 'identidade',
                    type: 'varchar',
                    length: '20',
                    isNullable: true
                }, {
                    name: 'criado_em',
                    type: 'date',
                    isNullable: false,
                    default: 'now()'
                }, {
                    name: 'atualizado_em',
                    type: 'date',
                    default: 'now()'
                }
            ]
        });
        await queryRunner.createTable(table, true);
        const fkUser = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_login_usuario'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
        });
        await queryRunner.createForeignKeys('pessoa', [fkUser]);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('pessoa');
    }
}
exports.createTablePessoa1599687456147 = createTablePessoa1599687456147;
