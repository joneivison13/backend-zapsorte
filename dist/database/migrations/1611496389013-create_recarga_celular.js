"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createRecargaCelular1611496389013 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'recarga_celular',
            columns: [
                {
                    name: 'id_recarga_celular',
                    type: 'int',
                    isPrimary: true,
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: 'increment'
                }, {
                    name: 'operadora',
                    type: 'varchar',
                    isNullable: false
                }, {
                    name: 'valor_recarga',
                    type: 'decimal',
                    precision: 11,
                    scale: 2,
                    isNullable: false
                }, {
                    name: 'numero_celular',
                    type: 'varchar',
                    length: '11',
                    isNullable: false
                }, {
                    name: 'criado_em',
                    type: 'date',
                    isNullable: false,
                    default: 'now()'
                }, {
                    name: 'atualizado_em',
                    type: 'date',
                    default: 'now()'
                }
            ]
        });
        await queryRunner.createTable(table, true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('recarga_celular');
    }
}
exports.createRecargaCelular1611496389013 = createRecargaCelular1611496389013;
