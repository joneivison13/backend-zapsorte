"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createTableRegistroTitulo1599687796336 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'registro_titulo',
            columns: [{
                    name: 'id_registro_titulo',
                    type: 'int',
                    isPrimary: true,
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: 'increment'
                }, {
                    name: 'fk_id_login_usuario',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'fk_id_titulo',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'data_registro',
                    type: 'date',
                    isNullable: false
                }, {
                    name: 'ganhador',
                    type: 'varchar',
                    isNullable: false,
                    default: "'N'"
                }]
        });
        await queryRunner.createTable(table);
        const fkTitulo = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_titulo'],
            referencedColumnNames: ['id_titulo'],
            referencedTableName: 'titulo',
            onDelete: 'CASCADE'
        });
        const fkUsuarioApp = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_login_usuario'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
        });
        await queryRunner.createForeignKeys('registro_titulo', [fkTitulo, fkUsuarioApp]);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('registro_titulo');
    }
}
exports.createTableRegistroTitulo1599687796336 = createTableRegistroTitulo1599687796336;
