"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createTableTitulo1599687781376 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'titulo',
            columns: [{
                    name: 'id_titulo',
                    type: 'int',
                    isPrimary: true,
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: 'increment'
                }, {
                    name: 'fk_id_parceiro',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'numero_titulo',
                    type: 'varchar',
                    length: '400',
                    isNullable: false
                }, {
                    name: 'data_lancamento',
                    type: 'date',
                    isNullable: false
                }, {
                    name: 'data_vencimento',
                    type: 'date',
                    isNullable: false
                }]
        });
        await queryRunner.createTable(table);
        const fkUser = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_parceiro'],
            referencedColumnNames: ['id_parceiro'],
            referencedTableName: 'parceiro',
            onDelete: 'CASCADE'
        });
        await queryRunner.createForeignKey('titulo', fkUser);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('titulo');
    }
}
exports.createTableTitulo1599687781376 = createTableTitulo1599687781376;
