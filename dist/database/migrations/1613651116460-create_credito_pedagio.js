"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createCreditoPedagio1613651116460 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'credito_pedagio',
            columns: [{
                    name: 'id_credito_pedagio',
                    type: 'int',
                    isPrimary: true,
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: 'increment'
                }, {
                    name: 'fk_id_login_usuario',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'valor_credito_pedagio',
                    type: 'decimal',
                    precision: 11,
                    scale: 2,
                    isNullable: false
                }, {
                    name: 'criado_em',
                    type: 'date',
                    isNullable: false,
                    default: 'now()'
                }, {
                    name: 'atualizado_em',
                    type: 'date',
                    default: 'now()'
                }
            ]
        });
        await queryRunner.createTable(table, true);
        const fk = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_login_usuario'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
        });
        await queryRunner.createForeignKeys('credito_pedagio', [fk]);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('credito_pedagio');
    }
}
exports.createCreditoPedagio1613651116460 = createCreditoPedagio1613651116460;
