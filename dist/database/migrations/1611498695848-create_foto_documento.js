"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createFotoDocumento1611498695848 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'foto_documento',
            columns: [{
                    name: 'id_foto_documento',
                    type: 'int',
                    isPrimary: true,
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: 'increment'
                }, {
                    name: 'endereco_imagem',
                    type: 'varchar',
                    isNullable: false
                }, {
                    name: 'adicionado_em',
                    type: 'date',
                    isNullable: false,
                    default: 'now()',
                }]
        });
        await queryRunner.createTable(table);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('foto_documento');
    }
}
exports.createFotoDocumento1611498695848 = createFotoDocumento1611498695848;
