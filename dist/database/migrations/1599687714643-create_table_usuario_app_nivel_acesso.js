"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createTableUsuarioAppNivelAcesso1599687714643 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'usuario_app_nivel_acesso',
            columns: [{
                    name: 'fk_id_login_usuario',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'fk_id_nivel_acesso',
                    type: 'int',
                    isNullable: false
                }]
        });
        await queryRunner.createTable(table);
        const fkUsuarioApp = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_login_usuario'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
        });
        const fkNivelAcesso = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_nivel_acesso'],
            referencedColumnNames: ['id_nivel_acesso'],
            referencedTableName: 'nivel_acesso',
            onDelete: 'CASCADE'
        });
        await queryRunner.createForeignKeys('usuario_app_nivel_acesso', [fkUsuarioApp, fkNivelAcesso]);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('usuario_app_nivel_acesso');
    }
}
exports.createTableUsuarioAppNivelAcesso1599687714643 = createTableUsuarioAppNivelAcesso1599687714643;
