"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createCaixaParceiro1611497354677 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'caixa_parceiro',
            columns: [{
                    name: 'id_caixa_parceiro',
                    type: 'int',
                    isPrimary: true,
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: 'increment'
                }, {
                    name: 'fk_id_parceiro',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'identificador_caixa_parceiro',
                    type: 'varchar',
                    isNullable: false
                }, {
                    name: 'ativo',
                    type: 'boolean',
                    default: true,
                    isNullable: false
                }, {
                    name: 'criado_em',
                    type: 'date',
                    isNullable: false,
                    default: 'now()'
                }, {
                    name: 'atualizado_em',
                    type: 'date',
                    default: 'now()'
                }
            ]
        });
        await queryRunner.createTable(table, true);
        const fk = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_parceiro'],
            referencedColumnNames: ['id_parceiro'],
            referencedTableName: 'parceiro',
            onDelete: 'CASCADE'
        });
        await queryRunner.createForeignKeys('caixa_parceiro', [fk]);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('caixa_parceiro');
    }
}
exports.createCaixaParceiro1611497354677 = createCaixaParceiro1611497354677;
