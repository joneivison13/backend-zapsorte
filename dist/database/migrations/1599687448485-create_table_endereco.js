"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createTableEndereco1599687448485 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'endereco',
            columns: [
                {
                    name: 'id_endereco',
                    type: 'int',
                    isPrimary: true,
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: 'increment'
                }, {
                    name: 'cep',
                    type: 'varchar',
                    isNullable: false,
                    length: '8'
                }, {
                    name: 'logradouro',
                    type: 'varchar',
                    isNullable: false,
                    length: '200'
                }, {
                    name: 'bairro',
                    type: 'varchar',
                    length: '80',
                    isNullable: false
                }, {
                    name: 'localidade',
                    type: 'varchar',
                    length: '100',
                    isNullable: false
                }, {
                    name: 'uf',
                    type: 'varchar',
                    length: '2',
                    isNullable: false
                }
            ]
        });
        await queryRunner.createTable(table, true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('endereco');
    }
}
exports.createTableEndereco1599687448485 = createTableEndereco1599687448485;
