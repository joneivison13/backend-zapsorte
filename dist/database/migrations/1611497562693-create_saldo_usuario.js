"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createSaldoUsuario1611497562693 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'saldo_usuario',
            columns: [{
                    name: 'id_saldo_usuario',
                    type: 'int',
                    isPrimary: true,
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: 'increment'
                }, {
                    name: 'fk_id_login_usuario',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'valor_lancado',
                    type: 'decimal',
                    precision: 11,
                    scale: 2,
                    isNullable: false
                }, {
                    name: 'data_lancamento_saldo_usuario',
                    type: 'date',
                    isNullable: false,
                    default: 'now()'
                }, {
                    name: 'criado_em',
                    type: 'date',
                    isNullable: false,
                    default: 'now()'
                }, {
                    name: 'atualizado_em',
                    type: 'date',
                    default: 'now()'
                }
            ]
        });
        await queryRunner.createTable(table, true);
        const fk = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_login_usuario'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
        });
        await queryRunner.createForeignKeys('saldo_usuario', [fk]);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('saldo_usuario');
    }
}
exports.createSaldoUsuario1611497562693 = createSaldoUsuario1611497562693;
