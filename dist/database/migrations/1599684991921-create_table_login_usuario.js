"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createTableLoginUsuario1599684991921 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'login_usuario',
            columns: [
                {
                    name: 'id_login_usuario',
                    type: 'int',
                    isPrimary: true,
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: 'increment'
                }, {
                    name: 'avatar',
                    type: 'varchar',
                    isNullable: true,
                }, {
                    name: 'cpf',
                    type: 'varchar',
                    isNullable: true,
                    length: '11'
                }, {
                    name: 'numero_celular',
                    type: 'varchar',
                    isNullable: true,
                    length: '11'
                }, {
                    name: 'password_usuario',
                    type: 'varchar',
                    isNullable: false,
                    length: '255'
                }, {
                    name: 'ativado',
                    type: 'boolean',
                    isNullable: false
                }, {
                    name: 'saldo_atual',
                    type: 'decimal',
                    isNullable: false,
                    precision: 11,
                    scale: 2,
                    default: '0'
                }, {
                    name: 'saldo_pedagio',
                    type: 'decimal',
                    isNullable: false,
                    precision: 11,
                    scale: 2,
                    default: '0'
                }, {
                    name: 'email',
                    type: 'varchar',
                    isNullable: true,
                    length: '100'
                }, {
                    name: 'criado_em',
                    type: 'date',
                    isNullable: false,
                    default: 'now()'
                }, {
                    name: 'atualizado_em',
                    type: 'date',
                    default: 'now()'
                }
            ]
        });
        await queryRunner.createTable(table, true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('login_usuario');
    }
}
exports.createTableLoginUsuario1599684991921 = createTableLoginUsuario1599684991921;
