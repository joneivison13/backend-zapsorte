"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createFotoIdentificacaoLoginUsuario1611498808008 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'foto_identificacao_login_usuario',
            columns: [{
                    name: 'fk_id_foto_documento',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'fk_id_login_usuario',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'criado_em',
                    type: 'date',
                    isNullable: false,
                    default: 'now()'
                }, {
                    name: 'atualizado_em',
                    type: 'date',
                    default: 'now()'
                }
            ]
        });
        await queryRunner.createTable(table, true);
        const fk1 = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_foto_documento'],
            referencedColumnNames: ['id_foto_documento'],
            referencedTableName: 'foto_documento',
            onDelete: 'CASCADE'
        });
        const fk2 = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_login_usuario'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
        });
        await queryRunner.createForeignKeys('foto_identificacao_login_usuario', [fk1, fk2]);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('foto_identificacao_login_usuario');
    }
}
exports.createFotoIdentificacaoLoginUsuario1611498808008 = createFotoIdentificacaoLoginUsuario1611498808008;
