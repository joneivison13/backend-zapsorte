"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createTableParceiro1599687763659 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'parceiro',
            columns: [
                {
                    name: 'id_parceiro',
                    type: 'int',
                    isPrimary: true,
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: 'increment'
                }, {
                    name: 'razao_social',
                    type: 'varchar',
                    length: '120',
                    isNullable: false
                }, {
                    name: 'nome_fantasia',
                    type: 'varchar',
                    length: '120',
                    isNullable: true
                }, {
                    name: 'cnpj',
                    type: 'varchar',
                    length: '14',
                    isNullable: false
                }, {
                    name: 'ativado',
                    type: 'boolean',
                    default: true,
                    isNullable: false
                }, {
                    name: 'telefone_contato_1',
                    type: 'varchar',
                    length: '11',
                    isNullable: false
                }, {
                    name: 'telefone_contato_2',
                    type: 'varchar',
                    length: '11',
                    isNullable: true
                }, {
                    name: 'telefone_contato_3',
                    type: 'varchar',
                    length: '11',
                    isNullable: true
                }, {
                    name: 'telefone_contato_4',
                    type: 'varchar',
                    length: '11',
                    isNullable: true
                }, {
                    name: 'email_1',
                    type: 'varchar',
                    length: '100',
                    isNullable: false
                }, {
                    name: 'email_2',
                    type: 'varchar',
                    length: '100',
                    isNullable: true
                }, {
                    name: 'email_3',
                    type: 'varchar',
                    length: '100',
                    isNullable: true
                }, {
                    name: 'email_4',
                    type: 'varchar',
                    length: '100',
                    isNullable: true
                }, {
                    name: 'pessoa_contato',
                    type: 'varchar',
                    length: '60',
                    isNullable: true
                }, {
                    name: 'celular_contato',
                    type: 'varchar',
                    length: '11',
                    isNullable: true
                }, {
                    name: 'email_contato',
                    type: 'varchar',
                    length: '100',
                    isNullable: true
                }, {
                    name: 'criado_em',
                    type: 'date',
                    isNullable: false,
                    default: 'now()'
                }, {
                    name: 'atualizado_em',
                    type: 'date',
                    default: 'now()'
                }
            ]
        });
        await queryRunner.createTable(table, true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('parceiro');
    }
}
exports.createTableParceiro1599687763659 = createTableParceiro1599687763659;
