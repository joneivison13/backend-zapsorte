"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createTipoMovimentacao1611499142814 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'tipo_movimentacao',
            columns: [{
                    name: 'id_tipo_movimentacao',
                    type: 'int',
                    isPrimary: true,
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: 'increment'
                }, {
                    name: 'descricao_tipo_movimentacao',
                    type: 'varchar',
                    isNullable: false
                }]
        });
        await queryRunner.createTable(table);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('tipo_movimentacao');
    }
}
exports.createTipoMovimentacao1611499142814 = createTipoMovimentacao1611499142814;
