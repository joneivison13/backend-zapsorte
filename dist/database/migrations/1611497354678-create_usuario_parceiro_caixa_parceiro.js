"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createUsuarioParceiroCaixaParceiro1611497354678 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'usuario_parceiro_caixa_parceiro',
            columns: [
                {
                    name: 'fk_id_nivel_acesso',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'fk_id_caixa_parceiro',
                    type: 'int',
                    isNullable: false
                }
            ]
        });
        await queryRunner.createTable(table, true);
        const fk1 = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_nivel_acesso'],
            referencedColumnNames: ['id_nivel_acesso'],
            referencedTableName: 'nivel_acesso',
            onDelete: 'CASCADE'
        });
        const fk2 = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_caixa_parceiro'],
            referencedColumnNames: ['id_caixa_parceiro'],
            referencedTableName: 'caixa_parceiro',
            onDelete: 'CASCADE'
        });
        await queryRunner.createForeignKeys('usuario_parceiro_caixa_parceiro', [fk1, fk2]);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('usuario_parceiro_caixa_parceiro');
    }
}
exports.createUsuarioParceiroCaixaParceiro1611497354678 = createUsuarioParceiroCaixaParceiro1611497354678;
