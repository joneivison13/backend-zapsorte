"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createBanco1611841497155 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'banco',
            columns: [{
                    name: 'id_banco',
                    type: 'int',
                    isPrimary: true,
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: 'increment'
                }, {
                    name: 'nome_banco',
                    type: 'varchar',
                    length: '100',
                    isNullable: false
                }, {
                    name: 'codigo_febrace',
                    type: 'int',
                    isNullable: false
                }]
        });
        await queryRunner.createTable(table);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('banco');
    }
}
exports.createBanco1611841497155 = createBanco1611841497155;
