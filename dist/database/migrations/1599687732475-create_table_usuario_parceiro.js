"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createTableUsuarioParceiro1599687732475 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'usuario_parceiro',
            columns: [
                {
                    name: 'id_usuario_parceiro',
                    type: 'int',
                    isPrimary: true,
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: 'increment'
                }, {
                    name: 'nome_usuario',
                    type: 'varchar',
                    length: '14'
                }, {
                    name: 'cpf',
                    type: 'varchar',
                    length: '14'
                }, {
                    name: 'password',
                    type: 'varchar',
                    isNullable: false,
                    length: '255'
                }, {
                    name: 'ativo',
                    type: 'boolean',
                    isNullable: false
                }, {
                    name: 'criado_em',
                    type: 'date',
                    isNullable: false,
                    default: 'now()'
                }, {
                    name: 'atualizado_em',
                    type: 'date',
                    default: 'now()'
                }
            ]
        });
        await queryRunner.createTable(table, true);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('usuario_parceiro');
    }
}
exports.createTableUsuarioParceiro1599687732475 = createTableUsuarioParceiro1599687732475;
