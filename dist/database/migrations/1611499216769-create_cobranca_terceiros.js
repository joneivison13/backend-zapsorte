"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createCobrancaTerceiros1611499216769 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'cobranca_terceiros',
            columns: [{
                    name: 'id_cobranca_terceiros',
                    type: 'int',
                    isPrimary: true,
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: 'increment'
                }, {
                    name: 'fk_usuario_entregador',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'fk_movimentacao',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'cobranca_terceiros',
                    type: 'date',
                    isNullable: false,
                    default: 'now()'
                }
            ]
        });
        await queryRunner.createTable(table, true);
        const fk1 = new typeorm_1.TableForeignKey({
            columnNames: ['fk_usuario_entregador'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
        });
        const fk2 = new typeorm_1.TableForeignKey({
            columnNames: ['fk_movimentacao'],
            referencedColumnNames: ['id_movimentacao_usuarios'],
            referencedTableName: 'movimentacao_usuarios',
            onDelete: 'CASCADE'
        });
        await queryRunner.createForeignKeys('cobranca_terceiros', [fk1, fk2]);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('cobranca_terceiros');
    }
}
exports.createCobrancaTerceiros1611499216769 = createCobrancaTerceiros1611499216769;
