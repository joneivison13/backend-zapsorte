"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createUsuarioParceiroParceiro1611497083535 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'usuario_parceiro_parceiro',
            columns: [
                {
                    name: 'fk_id_usuario_parceiro',
                    type: 'int',
                    isNullable: false
                }, {
                    name: 'fk_id_parceiro',
                    type: 'int',
                    isNullable: false
                }
            ]
        });
        await queryRunner.createTable(table, true);
        const fk1 = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_usuario_parceiro'],
            referencedColumnNames: ['id_usuario_parceiro'],
            referencedTableName: 'usuario_parceiro',
            onDelete: 'CASCADE'
        });
        const fk2 = new typeorm_1.TableForeignKey({
            columnNames: ['fk_id_parceiro'],
            referencedColumnNames: ['id_parceiro'],
            referencedTableName: 'parceiro',
            onDelete: 'CASCADE'
        });
        await queryRunner.createForeignKeys('usuario_parceiro_parceiro', [fk1, fk2]);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('usuario_parceiro_parceiro');
    }
}
exports.createUsuarioParceiroParceiro1611497083535 = createUsuarioParceiroParceiro1611497083535;
