"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
class createTableNivelAcesso1599687691907 {
    async up(queryRunner) {
        const table = new typeorm_1.Table({
            name: 'nivel_acesso',
            columns: [{
                    name: 'id_nivel_acesso',
                    type: 'int',
                    isPrimary: true,
                    isNullable: false,
                    isGenerated: true,
                    generationStrategy: 'increment'
                }, {
                    name: 'role',
                    type: 'varchar',
                    length: '100',
                    isNullable: false
                }]
        });
        await queryRunner.createTable(table);
    }
    async down(queryRunner) {
        await queryRunner.dropTable('nivel_acesso');
    }
}
exports.createTableNivelAcesso1599687691907 = createTableNivelAcesso1599687691907;
