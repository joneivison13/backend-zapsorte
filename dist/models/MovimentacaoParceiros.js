"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var MovimentacaoParceiros_1;
const typeorm_1 = require("typeorm");
const TipoMovimentacao_1 = __importDefault(require("./TipoMovimentacao"));
const LoginUsuario_1 = __importDefault(require("./LoginUsuario"));
let MovimentacaoParceiros = MovimentacaoParceiros_1 = class MovimentacaoParceiros {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_movimentacao_parceiros' }),
    __metadata("design:type", Number)
], MovimentacaoParceiros.prototype, "idMovimentacaoParceiros", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_tipo_movimentacao',
        nullable: false
    }),
    __metadata("design:type", Number)
], MovimentacaoParceiros.prototype, "fkIdTipoMovimentacao", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_login_usuario',
        nullable: false
    }),
    __metadata("design:type", Number)
], MovimentacaoParceiros.prototype, "fkIdLoginUsuario", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_parceiro',
        nullable: false
    }),
    __metadata("design:type", Number)
], MovimentacaoParceiros.prototype, "fkIdParceiro", void 0);
__decorate([
    typeorm_1.Column({
        name: 'descricao_movimentacao',
        nullable: false
    }),
    __metadata("design:type", String)
], MovimentacaoParceiros.prototype, "descricaoMovimentacao", void 0);
__decorate([
    typeorm_1.Column({
        name: 'valor_movimentacao',
        nullable: false,
        precision: 11,
        scale: 2
    }),
    __metadata("design:type", Number)
], MovimentacaoParceiros.prototype, "valorMovimentacao", void 0);
__decorate([
    typeorm_1.Column({
        name: 'data_movimentacao',
        nullable: false
    }),
    __metadata("design:type", Date)
], MovimentacaoParceiros.prototype, "dataMovimentacao", void 0);
__decorate([
    typeorm_1.ManyToOne(type => TipoMovimentacao_1.default, movimentacaoParceiros => MovimentacaoParceiros_1),
    typeorm_1.JoinColumn({ name: 'fk_id_tipo_movimentacao' }),
    __metadata("design:type", TipoMovimentacao_1.default)
], MovimentacaoParceiros.prototype, "tipoMovimentacao", void 0);
__decorate([
    typeorm_1.ManyToOne(type => LoginUsuario_1.default, movimentacaoParceiros => MovimentacaoParceiros_1),
    typeorm_1.JoinColumn({ name: 'fk_id_login_usuario' }),
    __metadata("design:type", TipoMovimentacao_1.default)
], MovimentacaoParceiros.prototype, "loginUsuario", void 0);
__decorate([
    typeorm_1.ManyToOne(type => LoginUsuario_1.default, movimentacaoParceiros => MovimentacaoParceiros_1),
    typeorm_1.JoinColumn({ name: 'fk_id_parceiro' }),
    __metadata("design:type", TipoMovimentacao_1.default)
], MovimentacaoParceiros.prototype, "parceiro", void 0);
__decorate([
    typeorm_1.CreateDateColumn({ name: 'criado_em' }),
    __metadata("design:type", Date)
], MovimentacaoParceiros.prototype, "criadoEm", void 0);
__decorate([
    typeorm_1.UpdateDateColumn({ name: 'atualizado_em' }),
    __metadata("design:type", Date)
], MovimentacaoParceiros.prototype, "atualizadoEm", void 0);
MovimentacaoParceiros = MovimentacaoParceiros_1 = __decorate([
    typeorm_1.Entity({ name: 'movimentacao_parceiros' })
], MovimentacaoParceiros);
exports.default = MovimentacaoParceiros;
