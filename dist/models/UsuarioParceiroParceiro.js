"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var UsuarioParceiroParceiro_1;
const typeorm_1 = require("typeorm");
const Parceiro_1 = __importDefault(require("./Parceiro"));
const UsuarioParceiro_1 = __importDefault(require("./UsuarioParceiro"));
let UsuarioParceiroParceiro = UsuarioParceiroParceiro_1 = class UsuarioParceiroParceiro {
};
__decorate([
    typeorm_1.Column({
        name: 'fk_id_usuario_parceiro',
        nullable: false,
        primary: true
    }),
    __metadata("design:type", Number)
], UsuarioParceiroParceiro.prototype, "fkIdUsuarioParceiro", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_parceiro',
        nullable: false,
        primary: true
    }),
    __metadata("design:type", Number)
], UsuarioParceiroParceiro.prototype, "fkIdParceiro", void 0);
__decorate([
    typeorm_1.ManyToOne(type => UsuarioParceiro_1.default, usuarioParceiroParceiro => UsuarioParceiroParceiro_1),
    typeorm_1.JoinColumn({ name: 'fk_id_login_usuario' }),
    __metadata("design:type", UsuarioParceiro_1.default)
], UsuarioParceiroParceiro.prototype, "usuarioParceiro", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Parceiro_1.default, usuarioParceiroParceiro => UsuarioParceiroParceiro_1),
    typeorm_1.JoinColumn({ name: 'fk_id_login_usuario' }),
    __metadata("design:type", Parceiro_1.default)
], UsuarioParceiroParceiro.prototype, "parceiro", void 0);
UsuarioParceiroParceiro = UsuarioParceiroParceiro_1 = __decorate([
    typeorm_1.Entity({ name: 'usuario_parceiro_parceiro' })
], UsuarioParceiroParceiro);
exports.default = UsuarioParceiroParceiro;
