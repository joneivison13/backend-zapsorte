"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var CobrancaTerceiros_1;
const typeorm_1 = require("typeorm");
const LoginUsuario_1 = __importDefault(require("./LoginUsuario"));
const MovimentacaoUsuarios_1 = __importDefault(require("./MovimentacaoUsuarios"));
let CobrancaTerceiros = CobrancaTerceiros_1 = class CobrancaTerceiros {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_cobranca_terceiros' }),
    __metadata("design:type", Number)
], CobrancaTerceiros.prototype, "idCobrancaTerceiros", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_usuario_entregador',
        nullable: false
    }),
    __metadata("design:type", Number)
], CobrancaTerceiros.prototype, "fkIdUsuarioEntregador", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_movimentacao',
        nullable: false
    }),
    __metadata("design:type", Number)
], CobrancaTerceiros.prototype, "fkMovimentacao", void 0);
__decorate([
    typeorm_1.Column({
        name: 'cobranca_terceiros',
        nullable: false
    }),
    __metadata("design:type", Date)
], CobrancaTerceiros.prototype, "cobrancaTerceiros", void 0);
__decorate([
    typeorm_1.ManyToOne(type => LoginUsuario_1.default, cobrancaTerceiros => CobrancaTerceiros_1),
    typeorm_1.JoinColumn({ name: 'fk_id_usuario_entregador' }),
    __metadata("design:type", LoginUsuario_1.default)
], CobrancaTerceiros.prototype, "loginUsuario", void 0);
__decorate([
    typeorm_1.ManyToOne(type => MovimentacaoUsuarios_1.default, cobrancaTerceiros => CobrancaTerceiros_1),
    typeorm_1.JoinColumn({ name: 'fk_movimentacao' }),
    __metadata("design:type", MovimentacaoUsuarios_1.default)
], CobrancaTerceiros.prototype, "movimentacaoUsuarios", void 0);
__decorate([
    typeorm_1.CreateDateColumn({ name: 'criado_em' }),
    __metadata("design:type", Date)
], CobrancaTerceiros.prototype, "criadoEm", void 0);
__decorate([
    typeorm_1.UpdateDateColumn({ name: 'atualizado_em' }),
    __metadata("design:type", Date)
], CobrancaTerceiros.prototype, "atualizadoEm", void 0);
CobrancaTerceiros = CobrancaTerceiros_1 = __decorate([
    typeorm_1.Entity({ name: 'cobranca_terceiros' })
], CobrancaTerceiros);
exports.default = CobrancaTerceiros;
