"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const LoginUsuario_1 = __importDefault(require("./LoginUsuario"));
const UsuarioPin_1 = __importDefault(require("./UsuarioPin"));
let UsuarioPin = class UsuarioPin {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_usuario_pin' }),
    __metadata("design:type", Number)
], UsuarioPin.prototype, "idUsuarioPin", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_login_usuario',
        nullable: true
    }),
    __metadata("design:type", Number)
], UsuarioPin.prototype, "fkIdLoginUsuario", void 0);
__decorate([
    typeorm_1.Column({
        name: 'pin_usuario',
        unique: true,
        nullable: false
    }),
    __metadata("design:type", String)
], UsuarioPin.prototype, "pinUsuario", void 0);
__decorate([
    typeorm_1.Column({
        name: 'celular',
        nullable: false
    }),
    __metadata("design:type", String)
], UsuarioPin.prototype, "celular", void 0);
__decorate([
    typeorm_1.CreateDateColumn({ name: 'criado_em' }),
    __metadata("design:type", Date)
], UsuarioPin.prototype, "criadoEm", void 0);
__decorate([
    typeorm_1.UpdateDateColumn({ name: 'atualizado_em' }),
    __metadata("design:type", Date)
], UsuarioPin.prototype, "atualizadoEm", void 0);
__decorate([
    typeorm_1.ManyToOne(type => LoginUsuario_1.default, usuariosPin => UsuarioPin_1.default),
    typeorm_1.JoinColumn({ name: 'fk_id_login_usuario' }),
    __metadata("design:type", LoginUsuario_1.default)
], UsuarioPin.prototype, "loginUsuario", void 0);
UsuarioPin = __decorate([
    typeorm_1.Entity({ name: 'usuario_pin' })
], UsuarioPin);
exports.default = UsuarioPin;
