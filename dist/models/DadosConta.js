"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var DadosConta_1;
const typeorm_1 = require("typeorm");
const LoginUsuario_1 = __importDefault(require("./LoginUsuario"));
let DadosConta = DadosConta_1 = class DadosConta {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_dados_conta' }),
    __metadata("design:type", Number)
], DadosConta.prototype, "idDadosConta", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_login_usuario',
        nullable: false
    }),
    __metadata("design:type", Number)
], DadosConta.prototype, "fkIdLoginUsuario", void 0);
__decorate([
    typeorm_1.Column({
        nullable: false
    }),
    __metadata("design:type", String)
], DadosConta.prototype, "agencia", void 0);
__decorate([
    typeorm_1.Column({
        nullable: false
    }),
    __metadata("design:type", String)
], DadosConta.prototype, "conta", void 0);
__decorate([
    typeorm_1.Column({
        nullable: false
    }),
    __metadata("design:type", String)
], DadosConta.prototype, "dvConta", void 0);
__decorate([
    typeorm_1.ManyToOne(type => LoginUsuario_1.default, dadosConta => DadosConta_1),
    typeorm_1.JoinColumn({ name: 'fk_id_login_usuario' }),
    __metadata("design:type", LoginUsuario_1.default)
], DadosConta.prototype, "loginUsuario", void 0);
__decorate([
    typeorm_1.CreateDateColumn({ name: 'criado_em' }),
    __metadata("design:type", Date)
], DadosConta.prototype, "criadoEm", void 0);
__decorate([
    typeorm_1.UpdateDateColumn({ name: 'atualizado_em' }),
    __metadata("design:type", Date)
], DadosConta.prototype, "atualizadoEm", void 0);
DadosConta = DadosConta_1 = __decorate([
    typeorm_1.Entity({ name: 'dados_conta' })
], DadosConta);
exports.default = DadosConta;
