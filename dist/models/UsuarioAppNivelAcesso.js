"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var UsuarioAppNivelAcesso_1;
const typeorm_1 = require("typeorm");
const LoginUsuario_1 = __importDefault(require("./LoginUsuario"));
const NivelAcesso_1 = __importDefault(require("./NivelAcesso"));
let UsuarioAppNivelAcesso = UsuarioAppNivelAcesso_1 = class UsuarioAppNivelAcesso {
};
__decorate([
    typeorm_1.Column({
        name: 'fk_id_login_usuario',
        nullable: false,
        primary: true
    }),
    __metadata("design:type", Number)
], UsuarioAppNivelAcesso.prototype, "fkIdLoginUsuario", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_nivel_acesso',
        nullable: false,
        primary: true
    }),
    __metadata("design:type", Number)
], UsuarioAppNivelAcesso.prototype, "fkIdNivelAcesso", void 0);
__decorate([
    typeorm_1.ManyToOne(type => LoginUsuario_1.default, usuarioAppNiveisAcesso => UsuarioAppNivelAcesso_1),
    typeorm_1.JoinColumn({ name: 'fk_id_login_usuario' }),
    __metadata("design:type", LoginUsuario_1.default)
], UsuarioAppNivelAcesso.prototype, "usuarioApp", void 0);
__decorate([
    typeorm_1.ManyToOne(type => NivelAcesso_1.default, usuarioAppNiveisAcesso => UsuarioAppNivelAcesso_1),
    typeorm_1.JoinColumn({ name: 'fk_id_nivel_acesso' }),
    __metadata("design:type", NivelAcesso_1.default)
], UsuarioAppNivelAcesso.prototype, "nivelAcesso", void 0);
UsuarioAppNivelAcesso = UsuarioAppNivelAcesso_1 = __decorate([
    typeorm_1.Entity({ name: 'usuario_app_nivel_acesso' })
], UsuarioAppNivelAcesso);
exports.default = UsuarioAppNivelAcesso;
