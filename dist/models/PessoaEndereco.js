"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const Pessoa_1 = __importDefault(require("./Pessoa"));
const Endereco_1 = __importDefault(require("./Endereco"));
const PessoaEndereco_1 = __importDefault(require("./PessoaEndereco"));
let PessoaEndereco = class PessoaEndereco {
};
__decorate([
    typeorm_1.Column({
        name: "fk_id_pessoa",
        nullable: false,
        primary: true,
    }),
    __metadata("design:type", Number)
], PessoaEndereco.prototype, "fkIdPessoa", void 0);
__decorate([
    typeorm_1.Column({
        name: "fk_id_endereco",
        nullable: false,
        primary: true,
    }),
    __metadata("design:type", Number)
], PessoaEndereco.prototype, "fkIdEndereco", void 0);
__decorate([
    typeorm_1.Column({
        nullable: false,
    }),
    __metadata("design:type", String)
], PessoaEndereco.prototype, "complemento", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    __metadata("design:type", String)
], PessoaEndereco.prototype, "latitude", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    __metadata("design:type", String)
], PessoaEndereco.prototype, "longitude", void 0);
__decorate([
    typeorm_1.ManyToOne((type) => Pessoa_1.default, (pessoaEnderecos) => PessoaEndereco_1.default),
    typeorm_1.JoinColumn({ name: "fk_id_pessoa" }),
    __metadata("design:type", Pessoa_1.default)
], PessoaEndereco.prototype, "pessoas", void 0);
__decorate([
    typeorm_1.ManyToOne((type) => Endereco_1.default, (pessoaEnderecos) => PessoaEndereco_1.default),
    typeorm_1.JoinColumn({ name: "fk_id_endereco" }),
    __metadata("design:type", Endereco_1.default)
], PessoaEndereco.prototype, "enderecos", void 0);
__decorate([
    typeorm_1.CreateDateColumn({ name: "criado_em" }),
    __metadata("design:type", Date)
], PessoaEndereco.prototype, "criadoEm", void 0);
__decorate([
    typeorm_1.UpdateDateColumn({ name: "atualizado_em" }),
    __metadata("design:type", Date)
], PessoaEndereco.prototype, "atualizadoEm", void 0);
PessoaEndereco = __decorate([
    typeorm_1.Entity({ name: "pessoa_endereco" })
], PessoaEndereco);
exports.default = PessoaEndereco;
