"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var LoginUsuario_1;
const typeorm_1 = require("typeorm");
const UsuarioPin_1 = __importDefault(require("./UsuarioPin"));
const Pessoa_1 = __importDefault(require("./Pessoa"));
const UsuarioAppNivelAcesso_1 = __importDefault(require("./UsuarioAppNivelAcesso"));
const RegistroTitulo_1 = __importDefault(require("./RegistroTitulo"));
const RecargaCelularLoginUsuario_1 = __importDefault(require("./RecargaCelularLoginUsuario"));
const DadosConta_1 = __importDefault(require("./DadosConta"));
const SaldoUsuario_1 = __importDefault(require("./SaldoUsuario"));
const Indicacao_1 = __importDefault(require("./Indicacao"));
const MovimentacaoUsuarios_1 = __importDefault(require("./MovimentacaoUsuarios"));
const MovimentacaoParceiros_1 = __importDefault(require("./MovimentacaoParceiros"));
const CobrancaTerceiros_1 = __importDefault(require("./CobrancaTerceiros"));
const CreditoPedagio_1 = __importDefault(require("./CreditoPedagio"));
let LoginUsuario = LoginUsuario_1 = class LoginUsuario {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_login_usuario' }),
    __metadata("design:type", Number)
], LoginUsuario.prototype, "idLoginUsuario", void 0);
__decorate([
    typeorm_1.Column({
        length: 11,
        unique: true,
        name: 'cpf'
    }),
    __metadata("design:type", String)
], LoginUsuario.prototype, "cpf", void 0);
__decorate([
    typeorm_1.Column({
        length: 11,
        name: 'numero_celular'
    }),
    __metadata("design:type", String)
], LoginUsuario.prototype, "numeroCelular", void 0);
__decorate([
    typeorm_1.Column({
        name: 'password_usuario',
        nullable: false
    }),
    __metadata("design:type", String)
], LoginUsuario.prototype, "passwordUsuario", void 0);
__decorate([
    typeorm_1.Column({
        nullable: false
    }),
    __metadata("design:type", String)
], LoginUsuario.prototype, "email", void 0);
__decorate([
    typeorm_1.Column({
        nullable: false,
        default: true
    }),
    __metadata("design:type", Boolean)
], LoginUsuario.prototype, "ativado", void 0);
__decorate([
    typeorm_1.Column({
        nullable: false,
        precision: 11,
        scale: 2,
        name: 'saldo_atual'
    }),
    __metadata("design:type", Number)
], LoginUsuario.prototype, "saldoAtual", void 0);
__decorate([
    typeorm_1.Column({
        nullable: false,
        precision: 11,
        scale: 2,
        name: 'saldo_pedagio'
    }),
    __metadata("design:type", Number)
], LoginUsuario.prototype, "saldoPedagio", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true
    }),
    __metadata("design:type", String)
], LoginUsuario.prototype, "avatar", void 0);
__decorate([
    typeorm_1.OneToMany(type => Pessoa_1.default, loginUsuario => LoginUsuario_1),
    __metadata("design:type", Array)
], LoginUsuario.prototype, "pessoas", void 0);
__decorate([
    typeorm_1.OneToMany(type => UsuarioPin_1.default, loginUsuario => LoginUsuario_1),
    __metadata("design:type", Array)
], LoginUsuario.prototype, "usuariosPin", void 0);
__decorate([
    typeorm_1.OneToMany(type => UsuarioAppNivelAcesso_1.default, loginUsuario => LoginUsuario_1),
    __metadata("design:type", Array)
], LoginUsuario.prototype, "usuarioAppNiveisAcesso", void 0);
__decorate([
    typeorm_1.OneToMany(type => RegistroTitulo_1.default, loginUsuario => loginUsuario),
    __metadata("design:type", Array)
], LoginUsuario.prototype, "registrosTitulo", void 0);
__decorate([
    typeorm_1.OneToMany(type => RecargaCelularLoginUsuario_1.default, loginUsuario => loginUsuario),
    __metadata("design:type", Array)
], LoginUsuario.prototype, "recargasCelularLoginUsuario", void 0);
__decorate([
    typeorm_1.OneToMany(type => DadosConta_1.default, loginUsuario => loginUsuario),
    __metadata("design:type", Array)
], LoginUsuario.prototype, "dadosConta", void 0);
__decorate([
    typeorm_1.OneToMany(type => SaldoUsuario_1.default, loginUsuario => loginUsuario),
    __metadata("design:type", Array)
], LoginUsuario.prototype, "saldoUsuario", void 0);
__decorate([
    typeorm_1.OneToMany(type => CreditoPedagio_1.default, loginUsuario => loginUsuario),
    __metadata("design:type", Array)
], LoginUsuario.prototype, "creditoPedagio", void 0);
__decorate([
    typeorm_1.OneToMany(type => Indicacao_1.default, loginUsuario => loginUsuario),
    __metadata("design:type", Array)
], LoginUsuario.prototype, "indicador", void 0);
__decorate([
    typeorm_1.OneToMany(type => Indicacao_1.default, loginUsuario => loginUsuario),
    __metadata("design:type", Array)
], LoginUsuario.prototype, "indicado", void 0);
__decorate([
    typeorm_1.OneToMany(type => MovimentacaoUsuarios_1.default, loginUsuario => loginUsuario),
    __metadata("design:type", Array)
], LoginUsuario.prototype, "pagador", void 0);
__decorate([
    typeorm_1.OneToMany(type => MovimentacaoUsuarios_1.default, loginUsuario => loginUsuario),
    __metadata("design:type", Array)
], LoginUsuario.prototype, "recebedor", void 0);
__decorate([
    typeorm_1.OneToMany(type => MovimentacaoParceiros_1.default, loginUsuario => loginUsuario),
    __metadata("design:type", Array)
], LoginUsuario.prototype, "loginUsuario", void 0);
__decorate([
    typeorm_1.OneToMany(type => CobrancaTerceiros_1.default, loginUsuario => loginUsuario),
    __metadata("design:type", Array)
], LoginUsuario.prototype, "cobrancaTerceiros", void 0);
__decorate([
    typeorm_1.CreateDateColumn({ name: 'criado_em' }),
    __metadata("design:type", Date)
], LoginUsuario.prototype, "criadoEm", void 0);
__decorate([
    typeorm_1.UpdateDateColumn({ name: 'atualizado_em' }),
    __metadata("design:type", Date)
], LoginUsuario.prototype, "atualizadoEm", void 0);
LoginUsuario = LoginUsuario_1 = __decorate([
    typeorm_1.Entity({ name: 'login_usuario' })
], LoginUsuario);
exports.default = LoginUsuario;
