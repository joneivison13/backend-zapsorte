"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const LoginUsuario_1 = __importDefault(require("./LoginUsuario"));
const Pessoa_1 = __importDefault(require("./Pessoa"));
const PessoaEndereco_1 = __importDefault(require("./PessoaEndereco"));
let Pessoa = class Pessoa {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_pessoa' }),
    __metadata("design:type", Number)
], Pessoa.prototype, "idPessoa", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_login_usuario',
        nullable: false
    }),
    __metadata("design:type", Number)
], Pessoa.prototype, "fkIdLoginUsuario", void 0);
__decorate([
    typeorm_1.Column({
        name: 'nome_pessoa',
        nullable: false
    }),
    __metadata("design:type", String)
], Pessoa.prototype, "nomePessoa", void 0);
__decorate([
    typeorm_1.Column({
        name: 'ultimo_nome_pessoa',
        nullable: false
    }),
    __metadata("design:type", String)
], Pessoa.prototype, "ultimoNomePessoa", void 0);
__decorate([
    typeorm_1.Column({
        name: 'data_nascimento',
        nullable: true
    }),
    __metadata("design:type", Date)
], Pessoa.prototype, "dataNascimento", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true
    }),
    __metadata("design:type", String)
], Pessoa.prototype, "genero", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true
    }),
    __metadata("design:type", String)
], Pessoa.prototype, "identidade", void 0);
__decorate([
    typeorm_1.ManyToOne(type => LoginUsuario_1.default, pessoas => Pessoa_1.default),
    typeorm_1.JoinColumn({ name: 'fk_id_login_usuario' }),
    __metadata("design:type", Array)
], Pessoa.prototype, "loginUsuario", void 0);
__decorate([
    typeorm_1.OneToMany(type => PessoaEndereco_1.default, pessoas => Pessoa_1.default),
    __metadata("design:type", Array)
], Pessoa.prototype, "pessoaEnderecos", void 0);
__decorate([
    typeorm_1.CreateDateColumn({ name: 'criado_em' }),
    __metadata("design:type", Date)
], Pessoa.prototype, "criadoEm", void 0);
__decorate([
    typeorm_1.UpdateDateColumn({ name: 'atualizado_em' }),
    __metadata("design:type", Date)
], Pessoa.prototype, "atualizadoEm", void 0);
Pessoa = __decorate([
    typeorm_1.Entity({ name: 'pessoa' })
], Pessoa);
exports.default = Pessoa;
