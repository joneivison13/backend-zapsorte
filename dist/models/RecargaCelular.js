"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var RecargaCelular_1;
const typeorm_1 = require("typeorm");
const RecargaCelularLoginUsuario_1 = __importDefault(require("./RecargaCelularLoginUsuario"));
let RecargaCelular = RecargaCelular_1 = class RecargaCelular {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_recarga_celular' }),
    __metadata("design:type", Number)
], RecargaCelular.prototype, "idRecargaCelular", void 0);
__decorate([
    typeorm_1.Column({
        nullable: false
    }),
    __metadata("design:type", String)
], RecargaCelular.prototype, "operadora", void 0);
__decorate([
    typeorm_1.Column({
        name: 'valor_recarga',
        nullable: false,
        precision: 11,
        scale: 2
    }),
    __metadata("design:type", Number)
], RecargaCelular.prototype, "valorRecarga", void 0);
__decorate([
    typeorm_1.Column({
        name: 'numero_celular',
        nullable: false
    }),
    __metadata("design:type", String)
], RecargaCelular.prototype, "numeroCelular", void 0);
__decorate([
    typeorm_1.CreateDateColumn({ name: 'criado_em' }),
    __metadata("design:type", Date)
], RecargaCelular.prototype, "criadoEm", void 0);
__decorate([
    typeorm_1.UpdateDateColumn({ name: 'atualizado_em' }),
    __metadata("design:type", Date)
], RecargaCelular.prototype, "atualizadoEm", void 0);
__decorate([
    typeorm_1.OneToMany(type => RecargaCelularLoginUsuario_1.default, recargaCelular => RecargaCelular_1),
    __metadata("design:type", Array)
], RecargaCelular.prototype, "recargasCelularLoginUsuario", void 0);
RecargaCelular = RecargaCelular_1 = __decorate([
    typeorm_1.Entity({ name: 'recarga_celular' })
], RecargaCelular);
exports.default = RecargaCelular;
