"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var NivelAcesso_1;
const typeorm_1 = require("typeorm");
const UsuarioAppNivelAcesso_1 = __importDefault(require("./UsuarioAppNivelAcesso"));
const UsuarioParceiroNivelAcesso_1 = __importDefault(require("./UsuarioParceiroNivelAcesso"));
let NivelAcesso = NivelAcesso_1 = class NivelAcesso {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_nivel_acesso' }),
    __metadata("design:type", Number)
], NivelAcesso.prototype, "idNivelAcesso", void 0);
__decorate([
    typeorm_1.Column({
        nullable: false
    }),
    __metadata("design:type", String)
], NivelAcesso.prototype, "role", void 0);
__decorate([
    typeorm_1.OneToMany(type => UsuarioAppNivelAcesso_1.default, nivelAcesso => NivelAcesso_1),
    __metadata("design:type", Array)
], NivelAcesso.prototype, "usuarioAppNiveisAcesso", void 0);
__decorate([
    typeorm_1.OneToMany(type => UsuarioParceiroNivelAcesso_1.default, nivelAcesso => NivelAcesso_1),
    __metadata("design:type", Array)
], NivelAcesso.prototype, "usuarioParceiroNiveisAcesso", void 0);
NivelAcesso = NivelAcesso_1 = __decorate([
    typeorm_1.Entity({ name: 'nivel_acesso' })
], NivelAcesso);
exports.default = NivelAcesso;
