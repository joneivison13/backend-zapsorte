"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var UsuarioParceiro_1;
const typeorm_1 = require("typeorm");
const UsuarioParceiroNivelAcesso_1 = __importDefault(require("./UsuarioParceiroNivelAcesso"));
const UsuarioParceiroParceiro_1 = __importDefault(require("./UsuarioParceiroParceiro"));
let UsuarioParceiro = UsuarioParceiro_1 = class UsuarioParceiro {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_usuario_parceiro' }),
    __metadata("design:type", Number)
], UsuarioParceiro.prototype, "idUsuarioParceiro", void 0);
__decorate([
    typeorm_1.Column({
        length: 11,
        unique: true
    }),
    __metadata("design:type", String)
], UsuarioParceiro.prototype, "cpf", void 0);
__decorate([
    typeorm_1.Column({
        nullable: false
    }),
    __metadata("design:type", String)
], UsuarioParceiro.prototype, "password", void 0);
__decorate([
    typeorm_1.Column({
        default: true,
        nullable: false
    }),
    __metadata("design:type", Boolean)
], UsuarioParceiro.prototype, "ativo", void 0);
__decorate([
    typeorm_1.OneToMany(type => UsuarioParceiroNivelAcesso_1.default, usuarioParceiro => UsuarioParceiro_1),
    __metadata("design:type", Array)
], UsuarioParceiro.prototype, "usuarioParceiroNiveisAcesso", void 0);
__decorate([
    typeorm_1.OneToMany(type => UsuarioParceiroParceiro_1.default, usuarioParceiro => UsuarioParceiro_1),
    __metadata("design:type", Array)
], UsuarioParceiro.prototype, "usuariosParceiroParceiro", void 0);
__decorate([
    typeorm_1.CreateDateColumn({ name: 'criado_em' }),
    __metadata("design:type", Date)
], UsuarioParceiro.prototype, "criadoEm", void 0);
__decorate([
    typeorm_1.UpdateDateColumn({ name: 'atualizado_em' }),
    __metadata("design:type", Date)
], UsuarioParceiro.prototype, "atualizadoEm", void 0);
UsuarioParceiro = UsuarioParceiro_1 = __decorate([
    typeorm_1.Entity({ name: 'usuario_parceiro' })
], UsuarioParceiro);
exports.default = UsuarioParceiro;
