"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var UsuarioParceiroCaixaParceiro_1;
const typeorm_1 = require("typeorm");
const CaixaParceiro_1 = __importDefault(require("./CaixaParceiro"));
const NivelAcesso_1 = __importDefault(require("./NivelAcesso"));
let UsuarioParceiroCaixaParceiro = UsuarioParceiroCaixaParceiro_1 = class UsuarioParceiroCaixaParceiro {
};
__decorate([
    typeorm_1.Column({
        name: 'fk_id_caixa_parceiro',
        nullable: false,
        primary: true
    }),
    __metadata("design:type", Number)
], UsuarioParceiroCaixaParceiro.prototype, "fkIdCaixaParceiro", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_nivel_acesso',
        nullable: false,
        primary: true
    }),
    __metadata("design:type", Number)
], UsuarioParceiroCaixaParceiro.prototype, "fkIdNivelAcesso", void 0);
__decorate([
    typeorm_1.ManyToOne(type => CaixaParceiro_1.default, usuarioParceiroCaixaParceiro => UsuarioParceiroCaixaParceiro_1),
    typeorm_1.JoinColumn({ name: 'fk_id_usuario_parceiro' }),
    __metadata("design:type", CaixaParceiro_1.default)
], UsuarioParceiroCaixaParceiro.prototype, "caixaParceiro", void 0);
__decorate([
    typeorm_1.ManyToOne(type => NivelAcesso_1.default, usuarioParceiroCaixaParceiro => UsuarioParceiroCaixaParceiro_1),
    typeorm_1.JoinColumn({ name: 'fk_id_nivel_acesso' }),
    __metadata("design:type", NivelAcesso_1.default)
], UsuarioParceiroCaixaParceiro.prototype, "nivelAcesso", void 0);
UsuarioParceiroCaixaParceiro = UsuarioParceiroCaixaParceiro_1 = __decorate([
    typeorm_1.Entity({ name: 'usuario_parceiro_caixa_parceiro' })
], UsuarioParceiroCaixaParceiro);
exports.default = UsuarioParceiroCaixaParceiro;
