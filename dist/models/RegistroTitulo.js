"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var RegistroTitulo_1;
const typeorm_1 = require("typeorm");
const Titulo_1 = __importDefault(require("./Titulo"));
const LoginUsuario_1 = __importDefault(require("./LoginUsuario"));
let RegistroTitulo = RegistroTitulo_1 = class RegistroTitulo {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_registro_titulo' }),
    __metadata("design:type", Number)
], RegistroTitulo.prototype, "idRegistroTitulo", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_login_usuario',
        nullable: false
    }),
    __metadata("design:type", Number)
], RegistroTitulo.prototype, "fkIdLoginUsuario", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_titulo',
        nullable: false
    }),
    __metadata("design:type", Number)
], RegistroTitulo.prototype, "fkIdTitulo", void 0);
__decorate([
    typeorm_1.Column({
        name: 'data_registro',
        nullable: false
    }),
    __metadata("design:type", Date)
], RegistroTitulo.prototype, "dataRegistro", void 0);
__decorate([
    typeorm_1.Column({
        nullable: false
    }),
    __metadata("design:type", String)
], RegistroTitulo.prototype, "ganhador", void 0);
__decorate([
    typeorm_1.ManyToOne(type => LoginUsuario_1.default, registrosTitulo => RegistroTitulo_1),
    typeorm_1.JoinColumn({ name: 'fk_id_login_usuario' }),
    __metadata("design:type", LoginUsuario_1.default)
], RegistroTitulo.prototype, "loginUsuario", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Titulo_1.default, registrosTitulo => RegistroTitulo_1),
    typeorm_1.JoinColumn({ name: 'fk_id_titulo' }),
    __metadata("design:type", Titulo_1.default)
], RegistroTitulo.prototype, "titulo", void 0);
RegistroTitulo = RegistroTitulo_1 = __decorate([
    typeorm_1.Entity({ name: 'registro_titulo' })
], RegistroTitulo);
exports.default = RegistroTitulo;
