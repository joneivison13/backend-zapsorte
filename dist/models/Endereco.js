"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Endereco_1;
const typeorm_1 = require("typeorm");
const PessoaEndereco_1 = __importDefault(require("./PessoaEndereco"));
const ParceiroEndereco_1 = __importDefault(require("./ParceiroEndereco"));
let Endereco = Endereco_1 = class Endereco {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_endereco' }),
    __metadata("design:type", Number)
], Endereco.prototype, "idEndereco", void 0);
__decorate([
    typeorm_1.Column({
        length: 8,
        nullable: false,
        name: 'cep'
    }),
    __metadata("design:type", String)
], Endereco.prototype, "cep", void 0);
__decorate([
    typeorm_1.Column({
        nullable: false
    }),
    __metadata("design:type", String)
], Endereco.prototype, "logradouro", void 0);
__decorate([
    typeorm_1.Column({
        nullable: false
    }),
    __metadata("design:type", String)
], Endereco.prototype, "bairro", void 0);
__decorate([
    typeorm_1.Column({
        nullable: false
    }),
    __metadata("design:type", String)
], Endereco.prototype, "localidade", void 0);
__decorate([
    typeorm_1.Column({
        nullable: false,
        length: 2
    }),
    __metadata("design:type", String)
], Endereco.prototype, "uf", void 0);
__decorate([
    typeorm_1.OneToMany(type => PessoaEndereco_1.default, endereco => Endereco_1),
    __metadata("design:type", Array)
], Endereco.prototype, "pessoaEnderecos", void 0);
__decorate([
    typeorm_1.OneToMany(type => ParceiroEndereco_1.default, endereco => Endereco_1),
    __metadata("design:type", Array)
], Endereco.prototype, "ParceiroEndereco", void 0);
Endereco = Endereco_1 = __decorate([
    typeorm_1.Entity({ name: 'endereco' })
], Endereco);
exports.default = Endereco;
