"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var TipoMovimentacao_1;
const typeorm_1 = require("typeorm");
const MovimentacaoUsuarios_1 = __importDefault(require("./MovimentacaoUsuarios"));
const MovimentacaoParceiros_1 = __importDefault(require("./MovimentacaoParceiros"));
let TipoMovimentacao = TipoMovimentacao_1 = class TipoMovimentacao {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_tipo_movimentacao' }),
    __metadata("design:type", Number)
], TipoMovimentacao.prototype, "idTipoMovimentacao", void 0);
__decorate([
    typeorm_1.Column({
        name: 'descricao_tipo_movimentacao',
        nullable: false
    }),
    __metadata("design:type", String)
], TipoMovimentacao.prototype, "descricaoTipoMovimentacao", void 0);
__decorate([
    typeorm_1.OneToMany(type => MovimentacaoUsuarios_1.default, tipoMovimentacao => TipoMovimentacao_1),
    __metadata("design:type", Array)
], TipoMovimentacao.prototype, "movimentacoesUsuarios", void 0);
__decorate([
    typeorm_1.OneToMany(type => MovimentacaoParceiros_1.default, tipoMovimentacao => TipoMovimentacao_1),
    __metadata("design:type", Array)
], TipoMovimentacao.prototype, "movimentacoesParceiros", void 0);
TipoMovimentacao = TipoMovimentacao_1 = __decorate([
    typeorm_1.Entity({ name: 'tipo_movimentacao' })
], TipoMovimentacao);
exports.default = TipoMovimentacao;
