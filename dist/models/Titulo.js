"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Titulo_1;
const typeorm_1 = require("typeorm");
const Parceiro_1 = __importDefault(require("./Parceiro"));
const RegistroTitulo_1 = __importDefault(require("./RegistroTitulo"));
let Titulo = Titulo_1 = class Titulo {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_titulo' }),
    __metadata("design:type", Number)
], Titulo.prototype, "idTitulo", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_parceiro',
        nullable: false
    }),
    __metadata("design:type", Number)
], Titulo.prototype, "fkIdParceiro", void 0);
__decorate([
    typeorm_1.Column({
        name: 'numero_titulo',
        nullable: false
    }),
    __metadata("design:type", String)
], Titulo.prototype, "numeroTitulo", void 0);
__decorate([
    typeorm_1.Column({
        name: 'data_lancamento',
        nullable: false
    }),
    __metadata("design:type", Date)
], Titulo.prototype, "dataLancamento", void 0);
__decorate([
    typeorm_1.Column({
        name: 'data_vencimento',
        nullable: false
    }),
    __metadata("design:type", Date)
], Titulo.prototype, "dataVencimento", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Parceiro_1.default, titulos => Titulo_1),
    typeorm_1.JoinColumn({ name: 'fk_id_parceiro' }),
    __metadata("design:type", Parceiro_1.default)
], Titulo.prototype, "parceiro", void 0);
__decorate([
    typeorm_1.OneToMany(type => RegistroTitulo_1.default, titulo => Titulo_1),
    __metadata("design:type", Array)
], Titulo.prototype, "registrosTitulo", void 0);
Titulo = Titulo_1 = __decorate([
    typeorm_1.Entity({ name: 'titulo' })
], Titulo);
exports.default = Titulo;
