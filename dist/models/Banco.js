"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Banco_1;
const typeorm_1 = require("typeorm");
const MovimentacaoUsuarios_1 = __importDefault(require("./MovimentacaoUsuarios"));
const Transferencia_1 = __importDefault(require("./Transferencia"));
let Banco = Banco_1 = class Banco {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_banco' }),
    __metadata("design:type", Number)
], Banco.prototype, "idBanco", void 0);
__decorate([
    typeorm_1.Column({
        name: 'nome_banco',
        nullable: false
    }),
    __metadata("design:type", String)
], Banco.prototype, "nomeBanco", void 0);
__decorate([
    typeorm_1.Column({
        name: 'codigo_febrace',
        nullable: false
    }),
    __metadata("design:type", Number)
], Banco.prototype, "codigoFebrace", void 0);
__decorate([
    typeorm_1.OneToMany(type => Transferencia_1.default, banco => Banco_1),
    __metadata("design:type", Array)
], Banco.prototype, "transferencia", void 0);
__decorate([
    typeorm_1.OneToMany(type => MovimentacaoUsuarios_1.default, banco => Banco_1),
    __metadata("design:type", Array)
], Banco.prototype, "movimentacaoUsuarios", void 0);
Banco = Banco_1 = __decorate([
    typeorm_1.Entity({ name: 'banco' })
], Banco);
exports.default = Banco;
