"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Parceiro_1;
const typeorm_1 = require("typeorm");
const ParceiroEndereco_1 = __importDefault(require("./ParceiroEndereco"));
const CaixaParceiro_1 = __importDefault(require("./CaixaParceiro"));
const UsuarioParceiroParceiro_1 = __importDefault(require("./UsuarioParceiroParceiro"));
const MovimentacaoParceiros_1 = __importDefault(require("./MovimentacaoParceiros"));
const Titulo_1 = __importDefault(require("./Titulo"));
let Parceiro = Parceiro_1 = class Parceiro {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_parceiro' }),
    __metadata("design:type", Number)
], Parceiro.prototype, "idParceiro", void 0);
__decorate([
    typeorm_1.Column({
        name: 'razao_social',
        nullable: false
    }),
    __metadata("design:type", String)
], Parceiro.prototype, "razaoSocial", void 0);
__decorate([
    typeorm_1.Column({
        name: 'nome_fantasia',
        nullable: true
    }),
    __metadata("design:type", String)
], Parceiro.prototype, "nomeFantasia", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
        length: 14
    }),
    __metadata("design:type", String)
], Parceiro.prototype, "cnpj", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
        default: true
    }),
    __metadata("design:type", Boolean)
], Parceiro.prototype, "ativado", void 0);
__decorate([
    typeorm_1.Column({
        name: 'telefone_contato_1',
        nullable: false,
        length: 11
    }),
    __metadata("design:type", String)
], Parceiro.prototype, "telefoneContato1", void 0);
__decorate([
    typeorm_1.Column({
        name: 'telefone_contato_2',
        nullable: true,
        length: 11
    }),
    __metadata("design:type", String)
], Parceiro.prototype, "telefoneContato2", void 0);
__decorate([
    typeorm_1.Column({
        name: 'telefone_contato_3',
        nullable: true,
        length: 11
    }),
    __metadata("design:type", String)
], Parceiro.prototype, "telefoneContato3", void 0);
__decorate([
    typeorm_1.Column({
        name: 'telefone_contato_4',
        nullable: true,
        length: 11
    }),
    __metadata("design:type", String)
], Parceiro.prototype, "telefoneContato4", void 0);
__decorate([
    typeorm_1.Column({
        name: 'email_1',
        nullable: false
    }),
    __metadata("design:type", String)
], Parceiro.prototype, "email1", void 0);
__decorate([
    typeorm_1.Column({
        name: 'email_2',
        nullable: true
    }),
    __metadata("design:type", String)
], Parceiro.prototype, "email2", void 0);
__decorate([
    typeorm_1.Column({
        name: 'email_3',
        nullable: true
    }),
    __metadata("design:type", String)
], Parceiro.prototype, "email3", void 0);
__decorate([
    typeorm_1.Column({
        name: 'email_4',
        nullable: true
    }),
    __metadata("design:type", String)
], Parceiro.prototype, "email4", void 0);
__decorate([
    typeorm_1.Column({
        name: 'pessoa_contato',
        nullable: true
    }),
    __metadata("design:type", String)
], Parceiro.prototype, "pessoaContato", void 0);
__decorate([
    typeorm_1.Column({
        name: 'celular_contato',
        nullable: true,
        length: 11
    }),
    __metadata("design:type", String)
], Parceiro.prototype, "celularContato", void 0);
__decorate([
    typeorm_1.Column({
        name: 'email_contato',
        nullable: true
    }),
    __metadata("design:type", String)
], Parceiro.prototype, "emailContato", void 0);
__decorate([
    typeorm_1.OneToMany(type => Titulo_1.default, parceiro => Parceiro_1),
    __metadata("design:type", Array)
], Parceiro.prototype, "titulos", void 0);
__decorate([
    typeorm_1.OneToMany(type => ParceiroEndereco_1.default, parceiro => Parceiro_1),
    __metadata("design:type", Array)
], Parceiro.prototype, "parceiroEndereco", void 0);
__decorate([
    typeorm_1.OneToMany(type => CaixaParceiro_1.default, parceiro => Parceiro_1),
    __metadata("design:type", Array)
], Parceiro.prototype, "caixasParceiro", void 0);
__decorate([
    typeorm_1.OneToMany(type => UsuarioParceiroParceiro_1.default, parceiro => Parceiro_1),
    __metadata("design:type", Array)
], Parceiro.prototype, "usuariosParceiroParceiro", void 0);
__decorate([
    typeorm_1.OneToMany(type => MovimentacaoParceiros_1.default, parceiro => Parceiro_1),
    __metadata("design:type", Array)
], Parceiro.prototype, "movimentacoesParceiro", void 0);
__decorate([
    typeorm_1.CreateDateColumn({ name: 'criado_em' }),
    __metadata("design:type", Date)
], Parceiro.prototype, "criadoEm", void 0);
__decorate([
    typeorm_1.UpdateDateColumn({ name: 'atualizado_em' }),
    __metadata("design:type", Date)
], Parceiro.prototype, "atualizadoEm", void 0);
Parceiro = Parceiro_1 = __decorate([
    typeorm_1.Entity({ name: 'parceiro' })
], Parceiro);
exports.default = Parceiro;
