"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var MovimentacaoUsuarios_1;
const typeorm_1 = require("typeorm");
const TipoMovimentacao_1 = __importDefault(require("./TipoMovimentacao"));
const LoginUsuario_1 = __importDefault(require("./LoginUsuario"));
const CobrancaTerceiros_1 = __importDefault(require("./CobrancaTerceiros"));
const Banco_1 = __importDefault(require("./Banco"));
let MovimentacaoUsuarios = MovimentacaoUsuarios_1 = class MovimentacaoUsuarios {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_movimentacao_usuarios' }),
    __metadata("design:type", Number)
], MovimentacaoUsuarios.prototype, "idMovimentacaoUsuarios", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_tipo_movimentacao',
        nullable: false
    }),
    __metadata("design:type", Number)
], MovimentacaoUsuarios.prototype, "fkIdTipoMovimentacao", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_pagador',
        nullable: true
    }),
    __metadata("design:type", Number)
], MovimentacaoUsuarios.prototype, "fkPagador", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_recebedor',
        nullable: true
    }),
    __metadata("design:type", Number)
], MovimentacaoUsuarios.prototype, "fkRecebedor", void 0);
__decorate([
    typeorm_1.Column({
        name: 'descricao_movimentacao',
        nullable: true
    }),
    __metadata("design:type", String)
], MovimentacaoUsuarios.prototype, "descricaoMovimentacao", void 0);
__decorate([
    typeorm_1.Column({
        name: 'valor_movimentacao',
        nullable: false,
        precision: 11,
        scale: 2
    }),
    __metadata("design:type", Number)
], MovimentacaoUsuarios.prototype, "valorMovimentacao", void 0);
__decorate([
    typeorm_1.Column({
        name: 'data_movimentacao',
        nullable: false
    }),
    __metadata("design:type", Date)
], MovimentacaoUsuarios.prototype, "dataMovimentacao", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true
    }),
    __metadata("design:type", String)
], MovimentacaoUsuarios.prototype, "agencia", void 0);
__decorate([
    typeorm_1.Column({
        name: 'conta_corrente',
        nullable: true
    }),
    __metadata("design:type", String)
], MovimentacaoUsuarios.prototype, "contaCorrente", void 0);
__decorate([
    typeorm_1.Column({
        name: 'cpf_transferencia',
        nullable: true
    }),
    __metadata("design:type", String)
], MovimentacaoUsuarios.prototype, "cpfTransferencia", void 0);
__decorate([
    typeorm_1.Column({
        name: 'nome_transferencia',
        nullable: true
    }),
    __metadata("design:type", String)
], MovimentacaoUsuarios.prototype, "nomeTransferencia", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_banco',
        nullable: true
    }),
    __metadata("design:type", Number)
], MovimentacaoUsuarios.prototype, "fkBanco", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Banco_1.default, movimentacaoUsuarios => MovimentacaoUsuarios_1),
    typeorm_1.JoinColumn({ name: 'fk_banco' }),
    __metadata("design:type", Banco_1.default)
], MovimentacaoUsuarios.prototype, "banco", void 0);
__decorate([
    typeorm_1.ManyToOne(type => TipoMovimentacao_1.default, movimentacaoUsuarios => MovimentacaoUsuarios_1),
    typeorm_1.JoinColumn({ name: 'fk_id_tipo_movimentacao' }),
    __metadata("design:type", TipoMovimentacao_1.default)
], MovimentacaoUsuarios.prototype, "tipoMovimentacao", void 0);
__decorate([
    typeorm_1.ManyToOne(type => LoginUsuario_1.default, movimentacaoUsuarios => MovimentacaoUsuarios_1),
    typeorm_1.JoinColumn({ name: 'fk_pagador' }),
    __metadata("design:type", LoginUsuario_1.default)
], MovimentacaoUsuarios.prototype, "pagador", void 0);
__decorate([
    typeorm_1.ManyToOne(type => LoginUsuario_1.default, movimentacaoUsuarios => MovimentacaoUsuarios_1),
    typeorm_1.JoinColumn({ name: 'fk_recebedor' }),
    __metadata("design:type", LoginUsuario_1.default)
], MovimentacaoUsuarios.prototype, "recebedor", void 0);
__decorate([
    typeorm_1.OneToMany(type => CobrancaTerceiros_1.default, movimentacaoUsuarios => MovimentacaoUsuarios_1),
    __metadata("design:type", Array)
], MovimentacaoUsuarios.prototype, "cobrancaTerceiros", void 0);
MovimentacaoUsuarios = MovimentacaoUsuarios_1 = __decorate([
    typeorm_1.Entity({ name: 'movimentacao_usuarios' })
], MovimentacaoUsuarios);
exports.default = MovimentacaoUsuarios;
