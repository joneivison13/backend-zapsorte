"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var RecargaCelularLoginUsuario_1;
const typeorm_1 = require("typeorm");
const LoginUsuario_1 = __importDefault(require("./LoginUsuario"));
const RecargaCelular_1 = __importDefault(require("./RecargaCelular"));
let RecargaCelularLoginUsuario = RecargaCelularLoginUsuario_1 = class RecargaCelularLoginUsuario {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_recarga_celular_login_usuario' }),
    __metadata("design:type", Number)
], RecargaCelularLoginUsuario.prototype, "idRecargaCelularLoginUsuario", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_login_usuario',
        nullable: false,
    }),
    __metadata("design:type", Number)
], RecargaCelularLoginUsuario.prototype, "fkIdLoginUsuario", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_recarga_celular',
        nullable: false,
    }),
    __metadata("design:type", Number)
], RecargaCelularLoginUsuario.prototype, "fkIdRecargaCelular", void 0);
__decorate([
    typeorm_1.Column({
        nullable: false,
    }),
    __metadata("design:type", String)
], RecargaCelularLoginUsuario.prototype, "complemento", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    __metadata("design:type", String)
], RecargaCelularLoginUsuario.prototype, "latitude", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    __metadata("design:type", String)
], RecargaCelularLoginUsuario.prototype, "longitude", void 0);
__decorate([
    typeorm_1.ManyToOne(type => LoginUsuario_1.default, recargasCelularLoginUsuario => RecargaCelularLoginUsuario_1),
    typeorm_1.JoinColumn({ name: 'fk_id_login_usuario' }),
    __metadata("design:type", LoginUsuario_1.default)
], RecargaCelularLoginUsuario.prototype, "loginUsuario", void 0);
__decorate([
    typeorm_1.ManyToOne(type => RecargaCelular_1.default, recargasCelularLoginUsuario => RecargaCelularLoginUsuario_1),
    typeorm_1.JoinColumn({ name: 'fk_id_recarga_celular' }),
    __metadata("design:type", RecargaCelular_1.default)
], RecargaCelularLoginUsuario.prototype, "recargaCelular", void 0);
__decorate([
    typeorm_1.CreateDateColumn({ name: 'criado_em' }),
    __metadata("design:type", Date)
], RecargaCelularLoginUsuario.prototype, "criadoEm", void 0);
__decorate([
    typeorm_1.UpdateDateColumn({ name: 'atualizado_em' }),
    __metadata("design:type", Date)
], RecargaCelularLoginUsuario.prototype, "atualizadoEm", void 0);
RecargaCelularLoginUsuario = RecargaCelularLoginUsuario_1 = __decorate([
    typeorm_1.Entity({ name: 'recarga_celular_login_usuario' })
], RecargaCelularLoginUsuario);
exports.default = RecargaCelularLoginUsuario;
