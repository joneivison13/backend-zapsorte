"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Transferencia_1;
const typeorm_1 = require("typeorm");
const Banco_1 = __importDefault(require("./Banco"));
const LoginUsuario_1 = __importDefault(require("./LoginUsuario"));
const MovimentacaoUsuarios_1 = __importDefault(require("./MovimentacaoUsuarios"));
let Transferencia = Transferencia_1 = class Transferencia {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_transferencia' }),
    __metadata("design:type", Number)
], Transferencia.prototype, "idTransferencia", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_banco',
        nullable: false
    }),
    __metadata("design:type", Number)
], Transferencia.prototype, "fkIdBanco", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_login_usuario',
        nullable: false
    }),
    __metadata("design:type", Number)
], Transferencia.prototype, "fkIdLoginUsuario", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_movimentacao_usuarios',
        nullable: false
    }),
    __metadata("design:type", Number)
], Transferencia.prototype, "fkIdMovimentacaoUsuarios", void 0);
__decorate([
    typeorm_1.Column({
        name: 'conta_corrente',
        nullable: false
    }),
    __metadata("design:type", String)
], Transferencia.prototype, "contaCorrente", void 0);
__decorate([
    typeorm_1.Column({
        name: 'agencia',
        nullable: false
    }),
    __metadata("design:type", Number)
], Transferencia.prototype, "agencia", void 0);
__decorate([
    typeorm_1.Column({
        name: 'cpf_destino',
        nullable: false
    }),
    __metadata("design:type", String)
], Transferencia.prototype, "cpfDestino", void 0);
__decorate([
    typeorm_1.Column({
        name: 'nome_completo_destino',
        nullable: false
    }),
    __metadata("design:type", String)
], Transferencia.prototype, "nomeCompletoDestino", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Banco_1.default, transferencia => Transferencia_1),
    typeorm_1.JoinColumn({ name: 'fk_id_banco' }),
    __metadata("design:type", Banco_1.default)
], Transferencia.prototype, "banco", void 0);
__decorate([
    typeorm_1.ManyToOne(type => LoginUsuario_1.default, transferencia => Transferencia_1),
    typeorm_1.JoinColumn({ name: 'fk_id_login_usuario' }),
    __metadata("design:type", LoginUsuario_1.default)
], Transferencia.prototype, "loginUsuario", void 0);
__decorate([
    typeorm_1.ManyToOne(type => MovimentacaoUsuarios_1.default, transferencia => Transferencia_1),
    typeorm_1.JoinColumn({ name: 'fk_id_movimentacao_usuarios' }),
    __metadata("design:type", MovimentacaoUsuarios_1.default)
], Transferencia.prototype, "movimentacaoUsuarios", void 0);
Transferencia = Transferencia_1 = __decorate([
    typeorm_1.Entity({ name: 'transferencia' })
], Transferencia);
exports.default = Transferencia;
