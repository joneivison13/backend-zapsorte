"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var CaixaParceiro_1;
const typeorm_1 = require("typeorm");
const Parceiro_1 = __importDefault(require("./Parceiro"));
const UsuarioParceiroCaixaParceiro_1 = __importDefault(require("./UsuarioParceiroCaixaParceiro"));
let CaixaParceiro = CaixaParceiro_1 = class CaixaParceiro {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_caixa_parceiro' }),
    __metadata("design:type", Number)
], CaixaParceiro.prototype, "idCaixaParceiro", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_parceiro',
        nullable: false
    }),
    __metadata("design:type", String)
], CaixaParceiro.prototype, "fkIdParceiro", void 0);
__decorate([
    typeorm_1.Column({
        name: 'identificador_caixa_parceiro',
        nullable: false
    }),
    __metadata("design:type", String)
], CaixaParceiro.prototype, "identificadorCaixaParceiro", void 0);
__decorate([
    typeorm_1.Column({
        name: 'ativo',
        nullable: false,
        default: true
    }),
    __metadata("design:type", Boolean)
], CaixaParceiro.prototype, "ativo", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Parceiro_1.default, caixaParceiro => CaixaParceiro_1),
    typeorm_1.JoinColumn({ name: 'fk_id_parceiro' }),
    __metadata("design:type", Parceiro_1.default)
], CaixaParceiro.prototype, "parceiro", void 0);
__decorate([
    typeorm_1.OneToMany(type => UsuarioParceiroCaixaParceiro_1.default, caixaParceiro => CaixaParceiro_1),
    __metadata("design:type", Array)
], CaixaParceiro.prototype, "usuarioParceiroCaixaParceiro", void 0);
__decorate([
    typeorm_1.CreateDateColumn({ name: 'criado_em' }),
    __metadata("design:type", Date)
], CaixaParceiro.prototype, "criadoEm", void 0);
__decorate([
    typeorm_1.UpdateDateColumn({ name: 'atualizado_em' }),
    __metadata("design:type", Date)
], CaixaParceiro.prototype, "atualizadoEm", void 0);
CaixaParceiro = CaixaParceiro_1 = __decorate([
    typeorm_1.Entity({ name: 'caixa_parceiro' })
], CaixaParceiro);
exports.default = CaixaParceiro;
