"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ParceiroEndereco_1;
const typeorm_1 = require("typeorm");
const Parceiro_1 = __importDefault(require("./Parceiro"));
const Endereco_1 = __importDefault(require("./Endereco"));
let ParceiroEndereco = ParceiroEndereco_1 = class ParceiroEndereco {
};
__decorate([
    typeorm_1.Column({
        name: 'fk_id_parceiro',
        nullable: false,
        primary: true
    }),
    __metadata("design:type", Number)
], ParceiroEndereco.prototype, "fkIdParceiro", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_endereco',
        nullable: false,
        primary: true
    }),
    __metadata("design:type", Number)
], ParceiroEndereco.prototype, "fkIdEndereco", void 0);
__decorate([
    typeorm_1.Column({
        nullable: false,
    }),
    __metadata("design:type", String)
], ParceiroEndereco.prototype, "complemento", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    __metadata("design:type", String)
], ParceiroEndereco.prototype, "latitude", void 0);
__decorate([
    typeorm_1.Column({
        nullable: true,
    }),
    __metadata("design:type", String)
], ParceiroEndereco.prototype, "longitude", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Parceiro_1.default, parceiroEndereco => ParceiroEndereco_1),
    typeorm_1.JoinColumn({ name: 'fk_id_parceiro' }),
    __metadata("design:type", Parceiro_1.default)
], ParceiroEndereco.prototype, "parceiro", void 0);
__decorate([
    typeorm_1.ManyToOne(type => Endereco_1.default, parceiroEndereco => ParceiroEndereco_1),
    typeorm_1.JoinColumn({ name: 'fk_id_endereco' }),
    __metadata("design:type", Endereco_1.default)
], ParceiroEndereco.prototype, "enderecos", void 0);
__decorate([
    typeorm_1.CreateDateColumn({ name: 'criado_em' }),
    __metadata("design:type", Date)
], ParceiroEndereco.prototype, "criadoEm", void 0);
__decorate([
    typeorm_1.UpdateDateColumn({ name: 'atualizado_em' }),
    __metadata("design:type", Date)
], ParceiroEndereco.prototype, "atualizadoEm", void 0);
ParceiroEndereco = ParceiroEndereco_1 = __decorate([
    typeorm_1.Entity({ name: 'parceiro_endereco' })
], ParceiroEndereco);
exports.default = ParceiroEndereco;
