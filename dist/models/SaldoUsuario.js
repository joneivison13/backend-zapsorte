"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var SaldoUsuario_1;
const typeorm_1 = require("typeorm");
const LoginUsuario_1 = __importDefault(require("./LoginUsuario"));
let SaldoUsuario = SaldoUsuario_1 = class SaldoUsuario {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('increment', { name: 'id_saldo_usuario' }),
    __metadata("design:type", Number)
], SaldoUsuario.prototype, "idSaldoUsuario", void 0);
__decorate([
    typeorm_1.Column({
        name: 'fk_id_login_usuario',
        nullable: false
    }),
    __metadata("design:type", Number)
], SaldoUsuario.prototype, "fkIdLoginUsuario", void 0);
__decorate([
    typeorm_1.Column({
        name: 'valor_lancado',
        precision: 11,
        scale: 2,
        nullable: false
    }),
    __metadata("design:type", Number)
], SaldoUsuario.prototype, "valorLancado", void 0);
__decorate([
    typeorm_1.Column({
        name: 'data_lancamento_saldo_usuario',
        nullable: false
    }),
    __metadata("design:type", Date)
], SaldoUsuario.prototype, "dataLancamentoSaldoUsuario", void 0);
__decorate([
    typeorm_1.ManyToOne(type => LoginUsuario_1.default, saldoUsuario => SaldoUsuario_1),
    typeorm_1.JoinColumn({ name: 'fk_id_login_usuario' }),
    __metadata("design:type", LoginUsuario_1.default)
], SaldoUsuario.prototype, "loginUsuario", void 0);
__decorate([
    typeorm_1.CreateDateColumn({ name: 'criado_em' }),
    __metadata("design:type", Date)
], SaldoUsuario.prototype, "criadoEm", void 0);
__decorate([
    typeorm_1.UpdateDateColumn({ name: 'atualizado_em' }),
    __metadata("design:type", Date)
], SaldoUsuario.prototype, "atualizadoEm", void 0);
SaldoUsuario = SaldoUsuario_1 = __decorate([
    typeorm_1.Entity({ name: 'saldo_usuario' })
], SaldoUsuario);
exports.default = SaldoUsuario;
