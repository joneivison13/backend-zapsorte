"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config({ path: `${__dirname}/../../.env` });
exports.default = {
    app: {
        port: process.env.PORT,
        node_env: process.env.NODE_ENV,
        salt: process.env.APP_SALT,
        secret: process.env.APP_SECRET
    },
    services: {
        smszenvia: {
            url: process.env.ZENVIA_SMS_URL,
            account: process.env.ZENVIA_SMS_ACCOUNT,
            password: process.env.ZENVIA_SMS_PASSWORD
        },
        smsdev: {
            url: process.env.SMS_DEV_URL,
            token: process.env.SMS_DEV_TOKEN
        }
    },
    database: {
        host: process.env.DB_HOST,
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_DATABASE,
        port: process.env.DB_PORT
    },
    mail: {
        user_email: process.env.USER_MAIL,
        user_pass: process.env.USER_PASS,
        pop: process.env.POP,
        pop_port_imap: process.env.POP_PORT_IMAP,
        pop_port_pop3: process.env.POP_PORT_POP3,
        smtp: process.env.SMTP,
        smtp_port: process.env.SMTP_PORT,
    }
};
