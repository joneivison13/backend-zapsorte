"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const LoginUsuarioController_1 = __importDefault(require("../controllers/LoginUsuarioController"));
const UsuarioParceiroController_1 = __importDefault(require("../controllers/UsuarioParceiroController"));
const EnderecoController_1 = __importDefault(require("../controllers/EnderecoController"));
const SessionController_1 = __importDefault(require("../controllers/SessionController"));
const auth_1 = __importDefault(require("../middlewares/auth"));
const NivelAcessoController_1 = __importDefault(require("../controllers/NivelAcessoController"));
const TituloController_1 = __importDefault(require("../controllers/TituloController"));
const RegistroTituloController_1 = __importDefault(require("../controllers/RegistroTituloController"));
const UsuarioPinController_1 = __importDefault(require("../controllers/UsuarioPinController"));
const MovimentacaoUsuariosController_1 = __importDefault(require("../controllers/MovimentacaoUsuariosController"));
const BancoController_1 = __importDefault(require("../controllers/BancoController"));
const multer_1 = __importDefault(require("multer"));
const multer_2 = __importDefault(require("../config/multer"));
const SMSController_1 = __importDefault(require("../controllers/SMSController"));
const routes = express_1.Router();
routes.get("/", (request, response) => {
    response.send("Server running!!!");
});
routes.get("/endereco/:cep", EnderecoController_1.default.getByCep);
routes.get("/endereco-usuario/:idUsuario", EnderecoController_1.default.get);
routes.post("/usuario", LoginUsuarioController_1.default.store);
routes.get("/usuario-celular/:celularUsuario", LoginUsuarioController_1.default.getByCelular);
routes.get("/usuario-cpf/:cpfUsuario", LoginUsuarioController_1.default.getByCPF);
routes.post("/usuario-parceiro", UsuarioParceiroController_1.default.store);
routes.get("/usuario-parceiro-cnpj/:cnpj", UsuarioParceiroController_1.default.getByCNPJ);
routes.post("/sessions-app", SessionController_1.default.store);
routes.post("/sessions-parceiro", SessionController_1.default.storeParceiro);
routes.post("/pin", UsuarioPinController_1.default.generate);
routes.post("/validate-pin", UsuarioPinController_1.default.validate);
routes.post("/pin-recuperar-senha", UsuarioPinController_1.default.generateRecuperarSenha);
routes.post("/session-recuperar-senha", SessionController_1.default.sessionRecuperarSenha);
routes.use(auth_1.default);
routes.put("/edit-password-app", SessionController_1.default.updateLoginUsuario);
routes.put("/edit-password-parceiro", SessionController_1.default.updateUsuarioParceiro);
routes.post("/nivel-acesso", NivelAcessoController_1.default.store);
routes.get("/nivel-acesso", NivelAcessoController_1.default.get);
routes.put("/nivel-acesso", NivelAcessoController_1.default.update);
routes.delete("/nivel-acesso/:idNivelAcesso", NivelAcessoController_1.default.delete);
routes.post("/titulo", TituloController_1.default.store);
routes.get("/titulo", TituloController_1.default.get);
routes.put("/titulo", TituloController_1.default.update);
routes.delete("/titulo/:idTitulo", TituloController_1.default.delete);
routes.get("/usuario-parceiros", UsuarioParceiroController_1.default.getParceiros);
routes.get("/titulos-usuario/:fkIdLoginUsuario", RegistroTituloController_1.default.getByUsuario);
routes.get("/ganhadores", RegistroTituloController_1.default.getGanhadores);
routes.post("/registrar-titulo", RegistroTituloController_1.default.store);
routes.put("/usuario", LoginUsuarioController_1.default.update);
const upload = multer_1.default(multer_2.default).single("file");
routes.post("/usuario-upload", (req, res) => {
    upload(req, res, function (err) {
        if (err) {
            return res.status(400).send({ message: err.message });
        }
        else {
            const file = req.file;
            res.status(200).send({
                filename: file.filename,
                mimetype: file.mimetype,
                originalname: file.originalname,
                size: file.size,
                fieldname: file.fieldname,
            });
        }
    });
}, LoginUsuarioController_1.default.upload);
routes.post("/gerar-troco", MovimentacaoUsuariosController_1.default.store);
routes.post("/banco", BancoController_1.default.store);
routes.put("/banco", BancoController_1.default.update);
routes.delete("/banco/:idBanco", BancoController_1.default.delete);
routes.get("/banco", BancoController_1.default.get);
routes.post("/adicionar-saldo-pedagio", MovimentacaoUsuariosController_1.default.store);
routes.post("/pagar-com-qr-code", MovimentacaoUsuariosController_1.default.store);
routes.post("/pagar-sem-celular", MovimentacaoUsuariosController_1.default.store);
routes.post("/pagar-informe-recebedor", MovimentacaoUsuariosController_1.default.store);
routes.post("/historico", MovimentacaoUsuariosController_1.default.getHistorico);
routes.post("/transferencia", MovimentacaoUsuariosController_1.default.storeTransferencia);
routes.post("/gerar-troco-mensagem", SMSController_1.default.gerarTrocoNaoCadastrado);
exports.default = routes;
