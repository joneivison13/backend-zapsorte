"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const NivelAcessoRepository_1 = __importDefault(require("../repositories/NivelAcessoRepository"));
const UsuarioAppNivelAcessoRepository_1 = __importDefault(require("../repositories/UsuarioAppNivelAcessoRepository"));
const UsuarioParceiroNivelAcessoRepository_1 = __importDefault(require("../repositories/UsuarioParceiroNivelAcessoRepository"));
const typeorm_1 = require("typeorm");
class NivelAcessoService {
    async store(request, response) {
        try {
            const { role } = request.body;
            const repo = typeorm_1.getCustomRepository(NivelAcessoRepository_1.default);
            const nivelAcesso = await repo.save({
                role
            });
            response.json(nivelAcesso);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async update(request, response) {
        try {
            const { idNivelAcesso, role } = request.body;
            const repo = typeorm_1.getCustomRepository(NivelAcessoRepository_1.default);
            const { affected } = await repo.update(idNivelAcesso, {
                role
            });
            response.json({ affected });
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async get(request, response) {
        try {
            const repo = typeorm_1.getCustomRepository(NivelAcessoRepository_1.default);
            const resp = await repo.find();
            response.json(resp);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async delete(request, response) {
        try {
            const { idNivelAcesso } = request.params;
            const repo = typeorm_1.getCustomRepository(NivelAcessoRepository_1.default);
            const repoUsuApp = typeorm_1.getCustomRepository(UsuarioAppNivelAcessoRepository_1.default);
            const repoUsuPar = typeorm_1.getCustomRepository(UsuarioParceiroNivelAcessoRepository_1.default);
            const existApp = await repoUsuApp.findOne({ fkIdNivelAcesso: Number(idNivelAcesso) });
            if (existApp) {
                response.status(401).send('Existe usuário app associado a este nível de acesso');
            }
            const existPar = await repoUsuPar.findOne({ fkIdNivelAcesso: Number(idNivelAcesso) });
            if (existPar) {
                response.status(401).send('Existe usuário parceiro associado a este nível de acesso');
            }
            const resp = await repo.delete(idNivelAcesso);
            response.json(resp);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async getDefaultApp() {
        try {
            const repo = typeorm_1.getCustomRepository(NivelAcessoRepository_1.default);
            return await repo.findOne({ role: 'APP' });
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async getDefaultParceiro() {
        try {
            const repo = typeorm_1.getCustomRepository(NivelAcessoRepository_1.default);
            return await repo.findOne({ role: 'PARCEIRO' });
        }
        catch (err) {
            console.log(err.message);
        }
    }
}
exports.default = new NivelAcessoService();
