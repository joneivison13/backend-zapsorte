"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const UsuarioPinRepository_1 = __importDefault(require("../repositories/UsuarioPinRepository"));
const LoginUsuarioRepository_1 = __importDefault(require("../repositories/LoginUsuarioRepository"));
const typeorm_1 = require("typeorm");
const axios_1 = __importDefault(require("axios"));
const config_1 = __importDefault(require("../config"));
class UsuarioPinService {
    generatePin() {
        const pin = Math.floor(Math.random() * 9999);
        if (pin < 1000)
            return Number(String(pin) + '9');
        return pin;
    }
    async generate(request, response) {
        try {
            const { celular } = request.body;
            const repo = typeorm_1.getCustomRepository(UsuarioPinRepository_1.default);
            const repoU = typeorm_1.getCustomRepository(LoginUsuarioRepository_1.default);
            const exist = await repoU.find({ numeroCelular: celular });
            if (exist.length > 0)
                return response.json({ exist: true });
            const pinUsuario = this.generatePin();
            const { data } = await axios_1.default({
                url: config_1.default.services.smsdev.url + `/v1/send?key=${config_1.default.services.smsdev.token}&type=9&number=${celular}&msg=${encodeURIComponent(`Z-${pinUsuario} é o seu código de ativação no ZapTroco.`)}&refer=${encodeURIComponent('ZapTroco')}`,
                method: 'GET',
                headers: {}
            });
            const registro = await repo.save({
                fkIdLoginUsuario: null,
                pinUsuario: String(pinUsuario),
                celular
            });
            response.json({ registro, sms: data });
        }
        catch (err) {
            console.log(err);
            response.json({ err, msg: 'Houve uma imprevisto no envio do SMS, tente novamente mais tarde...' });
        }
    }
    async validate(request, response) {
        try {
            const { pinUsuario, celular } = request.body;
            const repo = typeorm_1.getCustomRepository(UsuarioPinRepository_1.default);
            const registro = await repo.find({ where: { celular, pinUsuario } });
            response.json({ valid: !!registro.length });
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async generateRecuperarSenha(request, response) {
        try {
            const { celular, cpf } = request.body;
            const repo = typeorm_1.getCustomRepository(UsuarioPinRepository_1.default);
            const repoU = typeorm_1.getCustomRepository(LoginUsuarioRepository_1.default);
            let existCelular = null;
            if (celular)
                existCelular = await repoU.findOne({ numeroCelular: celular });
            let existCPF = null;
            if (cpf)
                existCPF = await repoU.findOne({ cpf });
            if (!existCelular || !existCPF)
                return response.json({ notExist: true });
            if (cpf && celular) {
                if (existCelular.cpf !== existCPF.cpf)
                    return response.json({ notExist: true });
            }
            const pinUsuario = this.generatePin();
            const { data } = await axios_1.default({
                url: config_1.default.services.smsdev.url + `/v1/send?key=${config_1.default.services.smsdev.token}&type=9&number=${existCelular.numeroCelular}&msg=${encodeURIComponent(`Z-${pinUsuario} é o seu código de ativação no ZapTroco.`)}&refer=${encodeURIComponent('ZapTroco')}`,
                method: 'GET',
                headers: {}
            });
            const registro = await repo.save({
                fkIdLoginUsuario: existCelular.idLoginUsuario,
                pinUsuario: String(pinUsuario),
                celular: existCelular.numeroCelular
            });
            registro.cpf = existCelular.cpf;
            response.json({ registro, sms: data });
        }
        catch (err) {
            console.log(err);
            response.json({ err, msg: 'Houve uma imprevisto no envio do SMS, tente novamente mais tarde...' });
        }
    }
}
exports.default = new UsuarioPinService();
