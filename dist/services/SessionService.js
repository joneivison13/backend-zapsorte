"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const LoginUsuarioRepository_1 = __importDefault(require("../repositories/LoginUsuarioRepository"));
const UsuarioParceiroRepository_1 = __importDefault(require("../repositories/UsuarioParceiroRepository"));
const PessoaRepository_1 = __importDefault(require("../repositories/PessoaRepository"));
const typeorm_1 = require("typeorm");
const bcrypt_1 = __importDefault(require("bcrypt"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const config_1 = __importDefault(require("../config"));
class SessionService {
    async store(request, response) {
        try {
            const { cpfUsuario, password } = request.body;
            const repo = typeorm_1.getCustomRepository(LoginUsuarioRepository_1.default);
            const usuario = await repo.createQueryBuilder("login_usuario")
                .where("(cpf = :cpf OR numero_celular = :numeroCelular) AND ativado = true")
                .setParameters({ cpf: cpfUsuario, numeroCelular: cpfUsuario })
                .getOne();
            if (!usuario) {
                return response.status(404).send('Usuário não encontrado');
            }
            const isPasswordCorrect = await bcrypt_1.default.compare(password, usuario.passwordUsuario);
            if (!isPasswordCorrect) {
                return response.status(401).send('Senha incorreta');
            }
            delete usuario.passwordUsuario;
            delete usuario.criadoEm;
            delete usuario.atualizadoEm;
            const repoPessoa = typeorm_1.getCustomRepository(PessoaRepository_1.default);
            const pessoa = await repoPessoa.findOne({ fkIdLoginUsuario: usuario.idLoginUsuario });
            usuario.pessoa = pessoa;
            return response.json({
                usuario: usuario,
                token: jsonwebtoken_1.default.sign({ idUsuario: usuario.idLoginUsuario }, config_1.default.app.secret, {})
            });
        }
        catch (e) {
            if (e.message === 'No metadata for "LoginUsuario" was found.') {
                return response.status(500).json({
                    erro: true,
                    message: 'Favor alterar o arquivo ORMCONFIG na raiz do projeto.'
                });
            }
            console.log(e.message);
        }
    }
    async storeParceiro(request, response) {
        const { cnpj, password } = request.body;
        const repo = typeorm_1.getCustomRepository(UsuarioParceiroRepository_1.default);
        const usuario = await repo.findOne({ cpf: cnpj });
        if (!usuario) {
            return response.status(404).send('Usuário não encontrado');
        }
        const isPasswordCorrect = await bcrypt_1.default.compare(password, usuario.password);
        if (!isPasswordCorrect) {
            return response.status(401).send('Senha incorreta');
        }
        delete usuario.password;
        delete usuario.criadoEm;
        delete usuario.atualizadoEm;
        return response.json({
            usuario: usuario,
            token: jsonwebtoken_1.default.sign({ idUsuarioParceiro: usuario.idUsuarioParceiro }, config_1.default.app.secret, { expiresIn: '7d' })
        });
    }
    async updatePassApp(request, response) {
        const { idLoginUsuario, newPassword, confirmPassword } = request.body;
        const repo = typeorm_1.getCustomRepository(LoginUsuarioRepository_1.default);
        const usuario = await repo.findOne({ idLoginUsuario: idLoginUsuario });
        if (!usuario)
            return response.status(401).send('Usuário não existe');
        if (newPassword !== confirmPassword)
            return response.status(401).send('As senhas não são iguais');
        const passwordHash = await bcrypt_1.default.hash(newPassword, Number(config_1.default.app.salt));
        await repo.update(idLoginUsuario, { passwordUsuario: passwordHash });
        return response.json({ message: 'Senha alterada com sucesso' });
    }
    async updatePassParceiro(request, response) {
        const { idUsuarioParceiro, password, newPassword, confirmPassword } = request.body;
        const repo = typeorm_1.getCustomRepository(UsuarioParceiroRepository_1.default);
        const usuario = await repo.findOne(idUsuarioParceiro);
        if (!usuario)
            return response.status(401).send('Usuário não existe');
        if (newPassword !== confirmPassword)
            return response.status(401).send('As senhas não são iguais');
        const isPasswordCorrect = await bcrypt_1.default.compare(password, usuario.password);
        if (!isPasswordCorrect) {
            return response.status(401).send('Senha atual incorreta');
        }
        const passwordHash = await bcrypt_1.default.hash(newPassword, Number(config_1.default.app.salt));
        await repo.update(idUsuarioParceiro, { password: passwordHash });
        return response.json({ message: 'Senha alterada com sucesso' });
    }
    async sessionRecuperarSenha(request, response) {
        const { numeroCelular } = request.body;
        const repo = typeorm_1.getCustomRepository(LoginUsuarioRepository_1.default);
        const usuario = await repo.findOne({ numeroCelular });
        if (!usuario) {
            return response.status(404).send('Usuário não encontrado');
        }
        delete usuario.passwordUsuario;
        delete usuario.criadoEm;
        delete usuario.atualizadoEm;
        const repoPessoa = typeorm_1.getCustomRepository(PessoaRepository_1.default);
        const pessoa = await repoPessoa.findOne({ fkIdLoginUsuario: usuario.idLoginUsuario });
        usuario.pessoa = pessoa;
        return response.json({
            usuario: usuario,
            token: jsonwebtoken_1.default.sign({ idUsuario: usuario.idLoginUsuario }, config_1.default.app.secret, {})
        });
    }
}
exports.default = new SessionService();
