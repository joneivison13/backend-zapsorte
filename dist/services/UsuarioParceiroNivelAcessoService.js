"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const UsuarioParceiroNivelAcessoRepository_1 = __importDefault(require("../repositories/UsuarioParceiroNivelAcessoRepository"));
const typeorm_1 = require("typeorm");
class UsuarioParceiroNivelAcessoService {
    async store(fkIdNivelAcesso, fkIdUsuarioParceiro) {
        try {
            const repo = typeorm_1.getCustomRepository(UsuarioParceiroNivelAcessoRepository_1.default);
            await repo.delete({ fkIdUsuarioParceiro, fkIdNivelAcesso });
            return await repo.save({
                fkIdUsuarioParceiro,
                fkIdNivelAcesso
            });
        }
        catch (err) {
            console.log(err.message);
        }
    }
}
exports.default = new UsuarioParceiroNivelAcessoService();
