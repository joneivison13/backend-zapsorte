"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const MovimentacaoUsuariosRepository_1 = __importDefault(require("../repositories/MovimentacaoUsuariosRepository"));
const PessoaRepository_1 = __importDefault(require("../repositories/PessoaRepository"));
const TipoMovimentacaoRepository_1 = __importDefault(require("../repositories/TipoMovimentacaoRepository"));
const LoginUsuarioRepository_1 = __importDefault(require("../repositories/LoginUsuarioRepository"));
class MovimentacaoUsuariosService {
    async store(request, response) {
        try {
            const { fkIdTipoMovimentacao, fkPagador, fkRecebedor, descricaoMovimentacao, valorMovimentacao, dataMovimentacao } = request.body;
            const repo = typeorm_1.getCustomRepository(MovimentacaoUsuariosRepository_1.default);
            const repoU = typeorm_1.getCustomRepository(LoginUsuarioRepository_1.default);
            const repoT = typeorm_1.getCustomRepository(TipoMovimentacaoRepository_1.default);
            const pagador = await repoU.findOne({ idLoginUsuario: fkPagador });
            if (!pagador)
                return response.json({ menssage: 'Pagador inválido!' });
            const recebedor = await repoU.findOne({ idLoginUsuario: fkRecebedor });
            if (!recebedor)
                return response.json({ menssage: 'Recebedor inválido!' });
            if (pagador.saldoAtual < valorMovimentacao)
                return response.json({ menssage: 'Saldo insuficiente!' });
            const existeTipo = await repoT.findOne({ idTipoMovimentacao: fkIdTipoMovimentacao });
            if (!existeTipo)
                return response.json({ menssage: 'Tipo de movimentação inválido!' });
            return;
            const registro = await repo.save({
                fkIdTipoMovimentacao,
                fkPagador,
                fkRecebedor,
                descricaoMovimentacao,
                valorMovimentacao,
                dataMovimentacao
            });
            return response.json(registro);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async storeTransferencia(request, response) {
        try {
            const { fkIdTipoMovimentacao, fkPagador, fkRecebedor, descricaoMovimentacao, valorMovimentacao, dataMovimentacao, fkBanco, agencia, contaCorrente, cpfTransferencia, nomeTransferencia } = request.body;
            const repo = typeorm_1.getCustomRepository(MovimentacaoUsuariosRepository_1.default);
            const repoU = typeorm_1.getCustomRepository(LoginUsuarioRepository_1.default);
            const usu = await repoU.findOne({ idLoginUsuario: fkPagador });
            if (usu.cpf === cpfTransferencia)
                return response.json({ menssage: 'O CPF do recebedor não pode ser o seu!' });
            const registro = await repo.save({
                fkIdTipoMovimentacao,
                fkPagador,
                fkRecebedor,
                descricaoMovimentacao,
                valorMovimentacao,
                dataMovimentacao,
                fkBanco,
                agencia,
                contaCorrente,
                cpfTransferencia,
                nomeTransferencia
            });
            return response.json(registro);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async getHistorico(request, response) {
        try {
            const { dataIni, dataFim, fkIdLoginUsuario, cpf } = request.body;
            const repo = typeorm_1.getCustomRepository(MovimentacaoUsuariosRepository_1.default);
            const repoP = typeorm_1.getCustomRepository(PessoaRepository_1.default);
            let registro = await repo.createQueryBuilder('movimentacao_usuario')
                .innerJoinAndSelect('movimentacao_usuario.tipoMovimentacao', 'tipo_movimentacao')
                .innerJoinAndSelect('movimentacao_usuario.pagador', 'login_usuario')
                .leftJoinAndSelect('movimentacao_usuario.recebedor', 'login_usuario as recebedor')
                .leftJoinAndSelect('movimentacao_usuario.banco', 'banco')
                .where(`data_movimentacao BETWEEN '${dataIni}' AND '${dataFim}'`)
                .andWhere(`(fk_pagador = ${fkIdLoginUsuario} OR fk_recebedor = ${fkIdLoginUsuario} OR cpf_transferencia = '${cpf}')`)
                .orderBy(`movimentacao_usuario.data_movimentacao`, 'DESC')
                .getMany();
            let arrParceNew = [];
            let arrParse = JSON.parse(JSON.stringify(registro));
            if (arrParse.length > 0) {
                arrParse.map(async (item, i) => {
                    let objPessoa = await repoP.findOne({ where: { fkIdLoginUsuario: item.fkPagador } });
                    item.nomePessoa = objPessoa.nomePessoa + ' ' + objPessoa.ultimoNomePessoa;
                    item.nomePessoaRecebedor = '';
                    if (item.fkRecebedor) {
                        let objPessoaRecebedor = await repoP.findOne({ where: { fkIdLoginUsuario: item.fkRecebedor } });
                        item.nomePessoaRecebedor = objPessoaRecebedor.nomePessoa + ' ' + objPessoaRecebedor.ultimoNomePessoa;
                    }
                    delete item.pagador.passwordUsuario;
                    delete item.recebedor.passwordUsuario;
                    arrParceNew.push(item);
                    if (arrParse.length === arrParceNew.length)
                        return response.json(arrParceNew);
                });
            }
            else {
                return response.json(arrParceNew);
            }
        }
        catch (err) {
            console.log(err.message);
        }
    }
}
exports.default = new MovimentacaoUsuariosService();
