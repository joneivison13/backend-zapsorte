"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const UsuarioAppNivelAcessoRepository_1 = __importDefault(require("../repositories/UsuarioAppNivelAcessoRepository"));
const typeorm_1 = require("typeorm");
class UsuarioAppNivelAcessoService {
    async store(fkIdNivelAcesso, fkIdLoginUsuario) {
        try {
            const repo = typeorm_1.getCustomRepository(UsuarioAppNivelAcessoRepository_1.default);
            await repo.delete({ fkIdLoginUsuario, fkIdNivelAcesso });
            return await repo.save({
                fkIdLoginUsuario,
                fkIdNivelAcesso
            });
        }
        catch (err) {
            console.log(err.message);
        }
    }
}
exports.default = new UsuarioAppNivelAcessoService();
