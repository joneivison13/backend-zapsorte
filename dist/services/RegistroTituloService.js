"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const RegistroTituloRepository_1 = __importDefault(require("../repositories/RegistroTituloRepository"));
const TituloRepository_1 = __importDefault(require("../repositories/TituloRepository"));
const PessoaRepository_1 = __importDefault(require("../repositories/PessoaRepository"));
const typeorm_1 = require("typeorm");
class RegistroTituloService {
    async store(request, response) {
        try {
            const { fkIdLoginUsuario, code } = request.body;
            const repoT = typeorm_1.getCustomRepository(TituloRepository_1.default);
            const repoR = typeorm_1.getCustomRepository(RegistroTituloRepository_1.default);
            const titulo = await repoT.findOne({ where: { numeroTitulo: code } });
            if (!titulo)
                return response.status(404).send('Este título não foi cadastrado no ZapTroco.');
            const adquirido = await repoR.findOne({ where: { fkIdTitulo: titulo.idTitulo } });
            if (adquirido)
                return response.status(401).send('Este título ja foi adquirido');
            const registro = await repoR.save({
                fkIdLoginUsuario,
                fkIdTitulo: titulo.idTitulo,
                dataRegistro: new Date().toLocaleDateString().split('/').reverse().join('-')
            });
            return response.json(registro);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async getByUsuario(request, response) {
        try {
            const { fkIdLoginUsuario } = request.params;
            const repoR = typeorm_1.getCustomRepository(RegistroTituloRepository_1.default);
            const registro = await repoR.createQueryBuilder('registro_titulo')
                .innerJoinAndSelect('registro_titulo.titulo', 'titulo')
                .innerJoinAndSelect('titulo.parceiro', 'parceiro')
                .where(`fk_id_login_usuario = ${fkIdLoginUsuario}`)
                .getMany();
            response.json(registro);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async getGanhadores(request, response) {
        try {
            const repoR = typeorm_1.getCustomRepository(RegistroTituloRepository_1.default);
            const repoP = typeorm_1.getCustomRepository(PessoaRepository_1.default);
            var registro = await repoR.createQueryBuilder('registro_titulo')
                .innerJoinAndSelect('registro_titulo.titulo', 'titulo')
                .innerJoinAndSelect('titulo.parceiro', 'parceiro')
                .innerJoinAndSelect('registro_titulo.loginUsuario', 'loginUsuario')
                .where("registro_titulo.ganhador = 'S'")
                .getMany();
            var newRegistro = [];
            if (registro.length > 0) {
                for (var i = 0; i < registro.length; i = i + 1) {
                    const { nomePessoa } = await repoP.findOne({ fkIdLoginUsuario: registro[i].fkIdLoginUsuario });
                    newRegistro[i] = registro[i];
                    newRegistro[i].nome = nomePessoa;
                    delete newRegistro[i].loginUsuario.passwordUsuario;
                }
            }
            response.json(newRegistro);
        }
        catch (err) {
            console.log(err.message);
        }
    }
}
exports.default = new RegistroTituloService();
