"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const BancoRepository_1 = __importDefault(require("../repositories/BancoRepository"));
const TransferenciaRepository_1 = __importDefault(require("../repositories/TransferenciaRepository"));
const typeorm_1 = require("typeorm");
class BancoService {
    async store(request, response) {
        try {
            const { nomeBanco, codigoFebrace } = request.body;
            const repo = typeorm_1.getCustomRepository(BancoRepository_1.default);
            const exist = await repo.findOne({ codigoFebrace: codigoFebrace });
            if (exist)
                return response.json({ message: 'Este código febrace já existe!' });
            const banco = await repo.save({
                nomeBanco,
                codigoFebrace
            });
            response.json(banco);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async update(request, response) {
        try {
            const { idBanco, nomeBanco, codigoFebrace } = request.body;
            const repo = typeorm_1.getCustomRepository(BancoRepository_1.default);
            const { affected } = await repo.update(idBanco, {
                nomeBanco,
                codigoFebrace
            });
            response.json({ affected });
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async get(request, response) {
        try {
            const repo = typeorm_1.getCustomRepository(BancoRepository_1.default);
            const resp = await repo.find();
            response.json(resp);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async delete(request, response) {
        try {
            const { idBanco } = request.params;
            const repo = typeorm_1.getCustomRepository(BancoRepository_1.default);
            const repoT = typeorm_1.getCustomRepository(TransferenciaRepository_1.default);
            const existApp = await repoT.findOne({ fkIdBanco: Number(idBanco) });
            if (existApp) {
                response.status(401).send('Existe transferência associada a este banco!');
            }
            const resp = await repo.delete(idBanco);
            response.json(resp);
        }
        catch (err) {
            console.log(err.message);
        }
    }
}
exports.default = new BancoService();
