"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const PessoaRepository_1 = __importDefault(require("../repositories/PessoaRepository"));
const typeorm_1 = require("typeorm");
class ClienteService {
    async store(fkIdLoginUsuario, nomePessoa, ultimoNomePessoa, dataNascimento, identidade, genero) {
        try {
            const repoPessoa = typeorm_1.getCustomRepository(PessoaRepository_1.default);
            return await repoPessoa.save({
                fkIdLoginUsuario,
                nomePessoa,
                ultimoNomePessoa,
                dataNascimento,
                genero,
                identidade
            });
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async update(idPessoa, fkIdLoginUsuario, nomePessoa, ultimoNomePessoa, dataNascimento, identidade, genero) {
        try {
            const repoPessoa = typeorm_1.getCustomRepository(PessoaRepository_1.default);
            return await repoPessoa.save({
                idPessoa,
                fkIdLoginUsuario,
                nomePessoa,
                ultimoNomePessoa,
                dataNascimento,
                genero,
                identidade
            });
        }
        catch (err) {
            console.log(err.message);
        }
    }
}
exports.default = new ClienteService();
