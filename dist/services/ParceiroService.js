"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ParceiroRepository_1 = __importDefault(require("../repositories/ParceiroRepository"));
const typeorm_1 = require("typeorm");
const ParceiroEnderecoRepository_1 = __importDefault(require("../repositories/ParceiroEnderecoRepository"));
const EnderecoRepository_1 = __importDefault(require("../repositories/EnderecoRepository"));
class ParceiroService {
    async store(fkIdUsuarioParceiro, fkIdEndereco, razaoSocial, nomeFantasia, telefoneContato1, telefoneContato2, telefoneContato3, telefoneContato4, email1, email2, email3, email4, pessoaContato, celularContato, emailContato, latitude, longitude) {
        try {
            const repo = typeorm_1.getCustomRepository(ParceiroRepository_1.default);
            return await repo.save({
                fkIdUsuarioParceiro,
                fkIdEndereco,
                razaoSocial,
                nomeFantasia,
                telefoneContato1,
                telefoneContato2,
                telefoneContato3,
                telefoneContato4,
                email1,
                email2,
                email3,
                email4,
                pessoaContato,
                celularContato,
                emailContato,
                latitude,
                longitude
            });
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async get(request, response) {
        try {
            const repoPE = typeorm_1.getCustomRepository(ParceiroEnderecoRepository_1.default);
            const repoE = typeorm_1.getCustomRepository(EnderecoRepository_1.default);
            const repoP = typeorm_1.getCustomRepository(ParceiroRepository_1.default);
            const resp = await repoP.find();
            var newRegistro = [];
            if (resp.length > 0) {
                for (var i = 0; i < resp.length; i = i + 1) {
                    let endeJoin = await repoPE.findOne({ fkIdParceiro: resp[i].idParceiro });
                    newRegistro[i] = resp[i];
                    let endeCompl = null;
                    if (endeJoin) {
                        endeCompl = await repoE.findOne({ idEndereco: endeJoin.fkIdEndereco });
                        newRegistro[i].cep = endeCompl.cep;
                        newRegistro[i].logradouro = endeCompl.logradouro;
                        newRegistro[i].bairro = endeCompl.bairro;
                        newRegistro[i].localidade = endeCompl.localidade;
                        newRegistro[i].uf = endeCompl.uf;
                        newRegistro[i].complemento = endeJoin.complemento;
                        newRegistro[i].latitude = endeJoin.latitude;
                        newRegistro[i].longitude = endeJoin.longitude;
                    }
                }
            }
            return response.json(newRegistro);
        }
        catch (err) {
            console.log(err.message);
        }
    }
}
exports.default = new ParceiroService();
