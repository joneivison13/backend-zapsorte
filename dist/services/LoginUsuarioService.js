"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const LoginUsuarioRepository_1 = __importDefault(require("../repositories/LoginUsuarioRepository"));
const PessoaRepository_1 = __importDefault(require("../repositories/PessoaRepository"));
const PessoaService_1 = __importDefault(require("../services/PessoaService"));
const NivelAcessoService_1 = __importDefault(require("../services/NivelAcessoService"));
const UsuarioAppNivelAcessoService_1 = __importDefault(require("../services/UsuarioAppNivelAcessoService"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const typeorm_1 = require("typeorm");
const config_1 = __importDefault(require("../config"));
const EnderecoRepository_1 = __importDefault(require("../repositories/EnderecoRepository"));
const PessoaEnderecoRepository_1 = __importDefault(require("../repositories/PessoaEnderecoRepository"));
class LoginUsuarioService {
    async store(request, response) {
        try {
            const { cpfUsuario, celularUsuario, password, nome, ultimoNome, rgUsuario, nascimento, genero, email, idEndereco, cep, logradouro, complemento, bairro, localidade, uf, } = request.body;
            const repoLoginUsuario = typeorm_1.getCustomRepository(LoginUsuarioRepository_1.default);
            const exist = await repoLoginUsuario.findOne({
                where: [{ cpf: cpfUsuario }, { numeroCelular: celularUsuario }],
            });
            if (exist) {
                return response.status(401).send("Usuário já existe");
            }
            const nivelAcesso = await NivelAcessoService_1.default.getDefaultApp();
            if (!nivelAcesso) {
                response.status(500).send('Nível de acesso "APP" não encontrado');
            }
            const passwordHash = await bcrypt_1.default.hash(password, Number(config_1.default.app.salt));
            const loginUsuario = await repoLoginUsuario.save({
                cpf: cpfUsuario,
                email,
                numeroCelular: celularUsuario,
                passwordUsuario: passwordHash,
                ativado: true,
            });
            delete loginUsuario.passwordUsuario;
            const pessoa = await PessoaService_1.default.store(loginUsuario.idLoginUsuario, nome, ultimoNome, nascimento, "", genero);
            await UsuarioAppNivelAcessoService_1.default.store(nivelAcesso.idNivelAcesso, loginUsuario.idLoginUsuario);
            response.json({ loginUsuario, pessoa });
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async update(request, response) {
        try {
            const { idUsuario, cpfUsuario, nome, ultimoNome, celularUsuario, email, rgUsuario, nascimento, genero, idCliente, cep, logradouro, complemento, bairro, localidade, uf, latitude, longitude, loginUsuarioId, } = request.body;
            console.log(request.body);
            console.log(1);
            const repoLoginUsuario = typeorm_1.getCustomRepository(LoginUsuarioRepository_1.default);
            const repoEndereco = typeorm_1.getCustomRepository(EnderecoRepository_1.default);
            const repoPessoaEndereco = typeorm_1.getCustomRepository(PessoaEnderecoRepository_1.default);
            const existUsu = await repoLoginUsuario.findOne({
                where: [{ cpf: cpfUsuario }],
            });
            console.log(cpfUsuario, existUsu, +loginUsuarioId);
            if (!existUsu) {
                return response.status(401).send("Este CPF não existe na base.");
            }
            console.log(2);
            if (existUsu && +existUsu.idLoginUsuario !== +loginUsuarioId) {
                return response.status(401).send("Este CPF pertence a outra conta.");
            }
            console.log(3);
            let loginUsuario = await repoLoginUsuario.update({ idLoginUsuario: loginUsuarioId }, {
                cpf: cpfUsuario,
            });
            console.log(4);
            let existEndereco = await repoEndereco.findOne({
                where: [{ cep }],
            });
            console.log(5);
            if (!existEndereco) {
                console.log(5.1);
                existEndereco = await repoEndereco.save({
                    cep,
                    logradouro,
                    bairro,
                    localidade,
                    uf,
                });
            }
            console.log(6, existEndereco);
            let endereco = await repoPessoaEndereco.findOne({
                where: [{ fkIdPessoa: loginUsuarioId }],
            });
            let enderecoDb = false;
            console.log(7);
            if (!endereco) {
                console.log("insert", {
                    fkIdEndereco: existEndereco.idEndereco,
                    fkIdPessoa: loginUsuarioId,
                    complemento,
                    latitude,
                    longitude,
                });
                endereco = await repoPessoaEndereco.save({
                    fkIdEndereco: existEndereco.idEndereco,
                    fkIdPessoa: loginUsuarioId,
                    complemento,
                    latitude,
                    longitude,
                });
            }
            else {
                console.log("endereco id", existEndereco.idEndereco);
                endereco = await repoPessoaEndereco.update({
                    fkIdPessoa: loginUsuarioId,
                }, {
                    fkIdEndereco: existEndereco.idEndereco,
                    fkIdPessoa: loginUsuarioId,
                    complemento,
                    latitude,
                    longitude,
                });
            }
            console.log("end", endereco);
            loginUsuario.idUsuario = idUsuario;
            loginUsuario.cpfUsuario = cpfUsuario;
            loginUsuario.avatar = existUsu.avatar;
            loginUsuario.email = existUsu.email;
            loginUsuario.ativado = existUsu.ativado;
            loginUsuario.celularUsuario = celularUsuario;
            console.log(8);
            delete loginUsuario.generatedMaps;
            delete loginUsuario.raw;
            delete loginUsuario.affected;
            console.log(9);
            response.json({ usuario: loginUsuario, endereco, existEndereco });
        }
        catch (err) {
            console.log(err.message, err);
        }
    }
    async getByCelular(request, response) {
        try {
            const { celularUsuario } = request.params;
            const repoU = typeorm_1.getCustomRepository(LoginUsuarioRepository_1.default);
            const repoC = typeorm_1.getCustomRepository(PessoaRepository_1.default);
            const usuario = await repoU.findOne({
                numeroCelular: celularUsuario,
            });
            if (usuario) {
                delete usuario.passwordUsuario;
                const pessoa = await repoC.findOne({
                    fkIdLoginUsuario: usuario.idLoginUsuario,
                });
                usuario.pessoa = pessoa;
            }
            response.json(usuario);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async getByCPF(request, response) {
        try {
            const { cpfUsuario } = request.params;
            const repoU = typeorm_1.getCustomRepository(LoginUsuarioRepository_1.default);
            const repoC = typeorm_1.getCustomRepository(PessoaRepository_1.default);
            const usuario = await repoU.findOne({ cpf: cpfUsuario });
            if (usuario) {
                delete usuario.passwordUsuario;
                const pessoa = await repoC.findOne({
                    fkIdLoginUsuario: usuario.idLoginUsuario,
                });
                usuario.pessoa = pessoa;
            }
            response.json(usuario);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async upload(request, response) {
        try {
            const { filename } = request.file;
            const { idUsuario } = request.body;
            const repo = typeorm_1.getCustomRepository(LoginUsuarioRepository_1.default);
            await repo.update({ idLoginUsuario: idUsuario }, { avatar: filename });
            response.json({ filename });
        }
        catch (err) {
            console.log({ message: err.message });
        }
    }
}
exports.default = new LoginUsuarioService();
