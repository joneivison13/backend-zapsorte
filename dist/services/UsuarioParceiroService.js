"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const UsuarioParceiroRepository_1 = __importDefault(require("../repositories/UsuarioParceiroRepository"));
const ParceiroService_1 = __importDefault(require("../services/ParceiroService"));
const NivelAcessoService_1 = __importDefault(require("../services/NivelAcessoService"));
const UsuarioParceiroNivelAcessoService_1 = __importDefault(require("../services/UsuarioParceiroNivelAcessoService"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const typeorm_1 = require("typeorm");
const config_1 = __importDefault(require("../config"));
class UsuarioParceiroService {
    async store(request, response) {
        try {
            const { cnpj, password, razaoSocial, nomeFantasia, telefoneContato1, telefoneContato2, telefoneContato3, telefoneContato4, email1, email2, email3, email4, pessoaContato, celularContato, emailContato, idEndereco, cep, logradouro, complemento, bairro, localidade, uf, latitude, longitude } = request.body;
            const repoUsuarioParceiro = typeorm_1.getCustomRepository(UsuarioParceiroRepository_1.default);
            const exist = await repoUsuarioParceiro.findOne({
                where: [{ cnpj }]
            });
            if (exist) {
                return response.status(401).send('Usuário já existe');
            }
            const nivelAcesso = await NivelAcessoService_1.default.getDefaultParceiro();
            if (!nivelAcesso) {
                response.status(500).send('Nível de acesso "PARCEIRO" não encontrado');
            }
            const passwordHash = await bcrypt_1.default.hash(password, Number(config_1.default.app.salt));
            const usuarioParceiro = await repoUsuarioParceiro.save({
                cnpj,
                password: passwordHash,
                ativo: true
            });
            delete usuarioParceiro.password;
            const parceiro = await ParceiroService_1.default.store(usuarioParceiro.idUsuarioParceiro, null, razaoSocial, nomeFantasia, telefoneContato1, telefoneContato2, telefoneContato3, telefoneContato4, email1, email2, email3, email4, pessoaContato, celularContato, emailContato, latitude, longitude);
            await UsuarioParceiroNivelAcessoService_1.default.store(nivelAcesso.idNivelAcesso, usuarioParceiro.idUsuarioParceiro);
            response.json({ usuarioParceiro, parceiro });
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async getByCNPJ(request, response) {
        try {
            const { cnpj } = request.params;
            const repo = typeorm_1.getCustomRepository(UsuarioParceiroRepository_1.default);
            const usuario = await repo.findOne({ cpf: cnpj });
            if (usuario) {
                delete usuario.password;
            }
            response.json(usuario);
        }
        catch (err) {
            console.log(err.message);
        }
    }
}
exports.default = new UsuarioParceiroService();
