"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const config_1 = __importDefault(require("../config"));
class SMSService {
    async gerarTrocoNaoCadastrado(request, response) {
        try {
            const { nome, valor, celular } = request.body;
            const texto = `Baixe agora mesmo o Zaptroco e venha fazer parte de uma solução financeira feita para facilitar seu dia-a-dia!\n\nPassou troco: ${nome}\nValor troco: ${valor}\n\nhttp://www.zaptroco.com.br/zaptrocoapps`;
            const { data } = await axios_1.default({
                url: config_1.default.services.smsdev.url + `/v1/send?key=${config_1.default.services.smsdev.token}&type=9&number=${celular}&msg=${encodeURIComponent(texto)}&refer=${encodeURIComponent('ZapTroco')}`,
                method: 'GET',
                headers: {}
            });
            response.json({ data });
        }
        catch (err) {
            console.log(err);
            response.json({ err, msg: 'Houve uma imprevisto no envio do SMS, tente novamente mais tarde...' });
        }
    }
}
exports.default = new SMSService();
