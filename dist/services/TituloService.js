"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const TituloRepository_1 = __importDefault(require("../repositories/TituloRepository"));
const ParceiroRepository_1 = __importDefault(require("../repositories/ParceiroRepository"));
const typeorm_1 = require("typeorm");
const RegistroTituloRepository_1 = __importDefault(require("../repositories/RegistroTituloRepository"));
class TituloService {
    async store(request, response) {
        try {
            const { fkIdParceiro, numeroTitulo, dataLancamento, dataVencimento } = request.body;
            const repo = typeorm_1.getCustomRepository(TituloRepository_1.default);
            const repoU = typeorm_1.getCustomRepository(ParceiroRepository_1.default);
            const existU = await repoU.findOne({ idParceiro: fkIdParceiro });
            if (!existU)
                return response.json({ message: 'Este usuário não existe!' });
            const exist = await repo.findOne({ numeroTitulo: numeroTitulo, dataVencimento: dataVencimento });
            if (exist)
                return response.json({ message: 'Este título já existe!' });
            const titulo = await repo.save({
                fkIdParceiro, numeroTitulo, dataLancamento, dataVencimento
            });
            response.json(titulo);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async update(request, response) {
        try {
            const { idTitulo, numeroTitulo, dataLancamento, dataVencimento } = request.body;
            const repo = typeorm_1.getCustomRepository(TituloRepository_1.default);
            const { affected } = await repo.update(idTitulo, {
                numeroTitulo, dataLancamento, dataVencimento
            });
            response.json({ affected });
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async get(request, response) {
        try {
            const repo = typeorm_1.getCustomRepository(TituloRepository_1.default);
            const resp = await repo.find();
            response.json(resp);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async delete(request, response) {
        try {
            const { idTitulo } = request.params;
            const repo = typeorm_1.getCustomRepository(TituloRepository_1.default);
            const repoR = typeorm_1.getCustomRepository(RegistroTituloRepository_1.default);
            const existApp = await repoR.findOne({ fkIdTitulo: idTitulo });
            if (existApp) {
                return response.status(401).json({ message: 'Existe associação entre algum usuario e este título' });
            }
            await repo.delete(idTitulo);
            response.json({ message: 'Excluído com sucesso!' });
        }
        catch (err) {
            console.log(err.message);
        }
    }
}
exports.default = new TituloService();
