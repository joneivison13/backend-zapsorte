"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const nodemailer_1 = __importDefault(require("nodemailer"));
const config_1 = __importDefault(require("../config"));
class Mail {
    async send(subject, to, html) {
        const transporter = nodemailer_1.default.createTransport({
            host: config_1.default.mail.smtp,
            port: Number(config_1.default.mail.smtp_port),
            auth: {
                user: config_1.default.mail.user_email,
                pass: config_1.default.mail.user_pass
            }
        });
        transporter.sendMail({
            from: config_1.default.mail.user_email,
            to,
            subject,
            html
        }).then(info => {
            return info;
        }).catch(error => {
            console.log(error);
            return error;
        });
    }
}
exports.default = new Mail();
