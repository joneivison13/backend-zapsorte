"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const EnderecoRepository_1 = __importDefault(require("../repositories/EnderecoRepository"));
const PessoaEnderecoRepository_1 = __importDefault(require("../repositories/PessoaEnderecoRepository"));
const typeorm_1 = require("typeorm");
class EnderecoService {
    async store(cep, logradouro, bairro, localidade, uf) {
        try {
            const repoEndereco = typeorm_1.getCustomRepository(EnderecoRepository_1.default);
            let endereco = await repoEndereco.findOne({ cep });
            if (!endereco) {
                endereco = await repoEndereco.save({
                    cep,
                    logradouro,
                    bairro,
                    localidade,
                    uf,
                });
            }
            return endereco;
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async getByCep(request, response) {
        try {
            const { cep } = request.params;
            const repo = typeorm_1.getCustomRepository(EnderecoRepository_1.default);
            const endereco = await repo.findOne({ cep });
            if (!endereco) {
                const { data } = await axios_1.default.get("https://viacep.com.br/ws/" + cep + "/json/");
                response.json(data);
            }
            response.json(endereco);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async get(request, response) {
        try {
            const { idUsuario } = request.params;
            const repoEndereco = typeorm_1.getCustomRepository(EnderecoRepository_1.default);
            const repoPessoaEndereco = typeorm_1.getCustomRepository(PessoaEnderecoRepository_1.default);
            const pessoaEndereco = await repoPessoaEndereco.findOne({
                where: [{ fkIdPessoa: idUsuario }],
            });
            if (!pessoaEndereco) {
                return response.status(404).json({
                    error: true,
                    message: "Voçê ainda não cadastrou nenhum endereço.",
                });
            }
            const endereco = await repoEndereco.findOne({
                where: [{ idEndereco: pessoaEndereco.fkIdEndereco }],
            });
            endereco.complemento = pessoaEndereco.complemento;
            return response.json({ data: { endereco } });
        }
        catch (err) {
            console.log(err, Object.keys(err), err.name);
        }
    }
}
exports.default = new EnderecoService();
