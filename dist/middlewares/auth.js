"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const config_1 = __importDefault(require("../config"));
exports.default = (request, response, next) => {
    const authHeader = request.headers.authorization;
    if (!authHeader) {
        return response.status(401).send('Token não informado');
    }
    const [, token] = authHeader.split(' ');
    try {
        const payload = jsonwebtoken_1.default.verify(token, config_1.default.app.secret);
        request.idUsuario = payload.id;
        return next();
    }
    catch (err) {
        return response.status(401).send('Token inválido');
    }
};
