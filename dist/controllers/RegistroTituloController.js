"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const RegistroTituloService_1 = __importDefault(require("../services/RegistroTituloService"));
class RegistroTituloController {
    async store(request, response) {
        try {
            return RegistroTituloService_1.default.store(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async getByUsuario(request, response) {
        try {
            return RegistroTituloService_1.default.getByUsuario(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async getGanhadores(request, response) {
        try {
            return RegistroTituloService_1.default.getGanhadores(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
}
exports.default = new RegistroTituloController();
