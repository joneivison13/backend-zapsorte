"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const UsuarioPinService_1 = __importDefault(require("../services/UsuarioPinService"));
class UsuarioPinController {
    async generate(request, response) {
        try {
            return UsuarioPinService_1.default.generate(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async validate(request, response) {
        try {
            return UsuarioPinService_1.default.validate(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async generateRecuperarSenha(request, response) {
        try {
            return UsuarioPinService_1.default.generateRecuperarSenha(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
}
exports.default = new UsuarioPinController();
