"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const TransferenciaService_1 = __importDefault(require("../services/TransferenciaService"));
class TransferenciaController {
    async store(request, response) {
        try {
            return TransferenciaService_1.default.store(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
}
exports.default = new TransferenciaController();
