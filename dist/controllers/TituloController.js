"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const TituloService_1 = __importDefault(require("../services/TituloService"));
class TituloController {
    async store(request, response) {
        try {
            return TituloService_1.default.store(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async update(request, response) {
        try {
            return TituloService_1.default.update(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async get(request, response) {
        try {
            return TituloService_1.default.get(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async delete(request, response) {
        try {
            return TituloService_1.default.delete(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
}
exports.default = new TituloController();
