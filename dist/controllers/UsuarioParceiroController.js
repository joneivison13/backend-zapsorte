"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const UsuarioParceiroService_1 = __importDefault(require("../services/UsuarioParceiroService"));
const ParceiroService_1 = __importDefault(require("../services/ParceiroService"));
class UsuarioParceiroController {
    async store(request, response) {
        try {
            return UsuarioParceiroService_1.default.store(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async getByCNPJ(request, response) {
        try {
            return UsuarioParceiroService_1.default.getByCNPJ(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async getParceiros(request, response) {
        try {
            return ParceiroService_1.default.get(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
}
exports.default = new UsuarioParceiroController();
