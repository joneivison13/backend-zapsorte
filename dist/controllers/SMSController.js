"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const SMSService_1 = __importDefault(require("../services/SMSService"));
class SMSController {
    async gerarTrocoNaoCadastrado(request, response) {
        try {
            return SMSService_1.default.gerarTrocoNaoCadastrado(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
}
exports.default = new SMSController();
