"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const EnderecoService_1 = __importDefault(require("../services/EnderecoService"));
class EnderecoController {
    async getByCep(request, response) {
        return EnderecoService_1.default.getByCep(request, response);
    }
    async store(cep, logradouro, bairro, localidade, uf) {
        return EnderecoService_1.default.store(cep, logradouro, bairro, localidade, uf);
    }
    async get(request, response) {
        return EnderecoService_1.default.get(request, response);
    }
}
exports.default = new EnderecoController();
