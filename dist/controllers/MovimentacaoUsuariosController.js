"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const MovimentacaoUsuariosService_1 = __importDefault(require("../services/MovimentacaoUsuariosService"));
class MovimentacaoUsuarios {
    async store(request, response) {
        try {
            return MovimentacaoUsuariosService_1.default.store(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async storeTransferencia(request, response) {
        try {
            return MovimentacaoUsuariosService_1.default.storeTransferencia(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async getHistorico(request, response) {
        try {
            return MovimentacaoUsuariosService_1.default.getHistorico(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
}
exports.default = new MovimentacaoUsuarios();
