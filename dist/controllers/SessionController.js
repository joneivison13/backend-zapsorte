"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const SessionService_1 = __importDefault(require("../services/SessionService"));
class SessionController {
    async store(request, response) {
        return SessionService_1.default.store(request, response);
    }
    async storeParceiro(request, response) {
        return SessionService_1.default.storeParceiro(request, response);
    }
    async updateLoginUsuario(request, response) {
        return SessionService_1.default.updatePassApp(request, response);
    }
    async updateUsuarioParceiro(request, response) {
        return SessionService_1.default.updatePassParceiro(request, response);
    }
    async sessionRecuperarSenha(request, response) {
        return SessionService_1.default.sessionRecuperarSenha(request, response);
    }
}
exports.default = new SessionController();
