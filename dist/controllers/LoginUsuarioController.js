"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const LoginUsuarioService_1 = __importDefault(require("../services/LoginUsuarioService"));
class LoginUsuarioController {
    async store(request, response) {
        try {
            return LoginUsuarioService_1.default.store(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async update(request, response) {
        try {
            return LoginUsuarioService_1.default.update(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async getByCelular(request, response) {
        try {
            return LoginUsuarioService_1.default.getByCelular(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async getByCPF(request, response) {
        try {
            return LoginUsuarioService_1.default.getByCPF(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
    async upload(request, response) {
        try {
            return LoginUsuarioService_1.default.upload(request, response);
        }
        catch (err) {
            console.log(err.message);
        }
    }
}
exports.default = new LoginUsuarioController();
